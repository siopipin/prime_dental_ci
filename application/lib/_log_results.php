<br>
<hr>
<br>
<div class="nav">
    <ul class="nav">
        <li><a class="nav-link" href='#report-requests'>Requests</a></li>
        <li><a class="nav-link" href='#report-volume'>Volume</a></li>
        <li><a class="nav-link" href='#report-requests404'>Requests 404</a></li>
        <li><a class="nav-link" href='#report-ip'>IP</a></li>
        <li><a class="nav-link" href='#report-ua'>User Agents</a></li>
        <li><a class="nav-link" href='#report-suspicious-ua'>Suspicious User Agents</a></li>
    </ul>
</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load('visualization', '1', {
    'packages': ['corechart']
});
</script>
<br>
<hr>
<br>
<section id='report-requests'>
    <div class="page-header">
        <h3>ACCESS VIEW</h3>
    </div>
    <div id="histo_requests" class="container"></div>
    <?php $log->addHistoJS($log->getRequestsHisto(), 'Kunjungan', 'histo_requests', '#0000ff'); ?>
    <?php $log->displayTablereq($log->getRequestsList(), $log->getRequestsTotal(), 'Total Kunjungan', true); ?>
</section>
<br>
<hr>
<!-- <br>
<section id='report-requests404'>
    <div class="page-header">
        <h3>Kesalahan Akses (Not found 404)</h3>
    </div>
    <div id="histo_404" class="container"></div>
    <?php $log->addHistoJS($log->getRequests404Histo(), 'Requests', 'histo_404', '#ff0000'); ?>
    <?php $log->displayTable($log->getRequests404List(), $log->getRequests404Total(), 'Nb requests', true); ?>
</section>
<br>
<hr>
<br>
<section id='report-ip'>
    <div class="page-header">
        <h3>Alamat IP Pengunjung</h3>
    </div>
    <?php $log->displayTable($log->getIpList(), $log->getRequestsTotal(), 'Nb requests', false); ?>
</section>
<br>
<hr>
<br>
<section id='report-ua'>
    <div class="page-header">
        <h3>Informasi Browser atau User Agents</h3>
    </div>
    <?php $log->displayTable($log->getUaList(), $log->getRequestsTotal(), 'Nb requests', false); ?>
</section> -->