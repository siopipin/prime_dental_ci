<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Pages extends CI_Controller
{

	var $useModel = "Home";
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Home/' . $this->useModel . '_model');
	}


	public function view($num = '', $page = 'home')
	{
		if (!file_exists(APPPATH . 'views/pages/' . $page . '.php')) {
			show_404();
		}
		$data['title'] = ucfirst($page);

		// AMBIL DATA KATEGORI LAYANAN
		$data['kategorilayanan'] = $this->Home_model->kategorilayanan();

		// AMBIL DATA STOK / PRODUK

		$perpage = 8;
		$offset = $this->uri->segment(1);
		$data['allstok'] = $this->Home_model->getDataPagination($perpage, $offset)->result();
		$data['all'] = $this->Home_model->getAllProduct()->result();
		$data['cart'] = $this->cart->contents();

		$config['base_url'] = site_url();
		$config['total_rows'] = $this->Home_model->getAllProduct()->num_rows();
		$config['per_page'] = $perpage;
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$this->pagination->initialize($config);

		$this->load->view('beranda/components/header');
		$this->load->view('beranda/components/sub-header');

		$this->load->view('beranda/' . $page, $data);
		$this->load->view('beranda/components/footer');
	}

	public function produk($id = null)
	{
		// AMBIL DATA produk
		$data['produkdetail'] = $this->Home_model->produk($id);
		$data['cart'] = $this->cart->contents();


		$this->load->view('beranda/components/header');
		$this->load->view('beranda/components/sub-header');

		$this->load->view('beranda/detail-produk', $data);
		$this->load->view('beranda/components/footer');
	}

	public function detailtransaksi($id)
	{
		$data['title'] = 'detail transaksi';
		$data['detailtransaksi'] = $this->Home_model->detailtransaksi($id);
		$data['totalbayar'] = $this->Home_model->totalbayar($id);


		$this->load->view('beranda/components/header');
		$this->load->view('beranda/components/sub-header');

		$this->load->view('beranda/detail-transaksi', $data);
		$this->load->view('beranda/components/footer');
	}

	public function updatedetailtransaksi()
	{
		//Upload Image
		$config['upload_path'] = FCPATH . './assets/images/bukti/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['overwrite'] = true;
		$config['max_size'] = '10048';
		$config['max_width'] = '2000';
		$config['max_height'] = '2000';

		$this->load->library('upload', $config);

		$file_name = str_replace(' ', '_', $_FILES['userfile']['name']);

		if ($this->upload->do_upload('userfile')) {
			$post_image = $file_name;
		} else {
			$data['error'] = $this->upload->display_errors();
			$post_image = 'noimage.jpg';
		}

		$this->Home_model->updatetransaksi($post_image);

		//Set Message
		$this->session->set_flashdata(
			'success',
			'Data has been created Successfull.'
		);
		redirect('users/dashboard');
	}
}