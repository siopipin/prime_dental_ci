<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Cart extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('cart_model');
    }

    public function add_to_cart()
    { //fungsi Add To Cart
        $data = json_decode(file_get_contents('php://input'), true);

        $arr = array(
            'id' =>  $data['id_stok'],
            'name' => $data['nama'],
            'price' => $data['biaya'],
            'qty' =>  $data['quantity'],
        );
        $this->cart->insert($arr);
        echo $this->show_cart(); //tampilkan cart setelah added
    }

    public function show_cart()
    { //Fungsi untuk menampilkan Cart
        $output = '';
        $no = 0;
        foreach ($this->cart->contents() as $items) {
            $no++;
            $output .= '
                <tr>
                    <td>' . $items['name'] . '</td>
                    <td>' . number_format($items['price']) . '</td>
                    <td>' . $items['qty'] . '</td>
                    <td>' . number_format($items['subtotal']) . '</td>
                    <td><button type="button" id="' . $items['rowid'] . '" class="hapus_cart btn btn-danger btn-xs">Batal</button></td>
                </tr>
            ';
        }
        $output .= '
            <tr>
                <th colspan="3">Total</th>
                <th colspan="2">' . 'Rp ' . number_format($this->cart->total()) . '</th>
            </tr>
        ';
        return $output;
    }

    public  function load_cart()
    { //load data cart
        echo $this->show_cart();
    }

    public function hapus_cart()
    { //fungsi untuk menghapus item cart
        $data = array(
            'rowid' => $this->input->post('row_id'),
            'qty' => 0,
        );
        $this->cart->update($data);
        echo $this->show_cart();
    }

    //save transaksi
    public function save_transaksi()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $res = $this->cart_model->save_transaksi($data);

        if ($res) {
            foreach ($data['data'] as $key => $value) {
                $data = array(
                    'rowid' => $value['row_id'],
                    'qty' => 0,
                );
                $this->cart->update($data);
            }


            echo $res;
        } else {
            echo 0;
        }
    }
}