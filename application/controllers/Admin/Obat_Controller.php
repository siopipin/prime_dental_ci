<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Obat_Controller extends CI_Controller
{

    var $useModel = "Obat";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/' . $this->useModel . '_model');
    }

    // CONTROLLER VIEW DAN DATA OBAT
    public function obat($jenis)
    {
        // Pagination Config
        $config['base_url'] = base_url() . 'administrator/stok/obat/page/' . $jenis;

        // HITUNG TOTAL STOK DENGAN ID = 1 = OBAT;
        $config['total_rows'] = $this->Obat_model->get_total_obat($jenis);
        $config['per_page'] = 10;
        $config['uri_segment'] = 10;
        $config['attributes'] = ['class' => 'paginate-link'];

        // Init Pagination
        $this->pagination->initialize($config);

        $data['rows'] = $this->Obat_model->get_obat($jenis);
        $data['rowsjenis'] = $this->Obat_model->get_kategori();


        if ($jenis == '1') {
            $css['css'] = "stok_obat";
        } else if ($jenis == '2') {
            $css['css'] = "stok_produk";
        } else {
            $css['css'] = "stok_bahan";
        }

        $this->load->view('administrator/header-script');
        $this->load->view('administrator/header');
        $this->load->view('administrator/header-bottom', $css);
        $this->load->view('administrator/stok/obat', $data);
        $this->load->view('administrator/footer');
    }

    // ENABLE OBAT
    public function enable($id)
    {
        $table = base64_decode($this->input->get('table'));
        //$table = $this->input->post('table');
        $this->Obat_model->enable($id, $table);
        $this->session->set_flashdata('success', 'Desabled Successfully.');
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    // DISABLE OBAT
    public function disable($id)
    {
        $table = base64_decode($this->input->get('table'));
        //$table = $this->input->post('table');
        $this->Obat_model->disable($id, $table);
        $this->session->set_flashdata('success', 'Desabled Successfully.');
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    // VIEW EDIT OBAT
    public function edit_obat($id = null)
    {
        $data['row'] = $this->Obat_model->get_obat_byid($id);
        $data['kategori'] = $this->Obat_model->get_kategori();


        if (empty($data['row'])) {
            show_404();
        }
        $css['css'] = 'stok_obat';

        $this->load->view('administrator/header-script');
        $this->load->view('administrator/header');
        $this->load->view('administrator/header-bottom', $css);
        $this->load->view('administrator/stok/edit-obat', $data);
        $this->load->view('administrator/footer');
    }

    // SAVE EDIT OBAT
    public function save_obat_byid($page = 'stok/edit-obat')
    {

        // Check login
        if (!$this->session->userdata('login')) {
            redirect('administrator/index');
        }

        $data['title'] = 'Simpan Obat';


        // VALIDASI
        $this->form_validation->set_rules('nama', 'Nama Obat', 'required');
        $this->form_validation->set_rules('sku', 'SKU Obat', 'required');

        $css['css'] = 'x';


        if ($this->form_validation->run() === false) {
            $this->load->view('administrator/header-script');
            $this->load->view('administrator/header');
            $this->load->view('administrator/header-bottom', $css);
            $this->load->view('administrator/' . $page, $data);
            $this->load->view('administrator/footer');
        } else {
            //Upload Image

            $config['upload_path'] = './assets/images/products';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2048';
            $config['max_width'] = '2000';
            $config['max_height'] = '2000';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $id = $this->input->post('id');

                $data['img'] = $this->Obat_model->get_obat_byid($id);
                $post_image = $data['img']['img'];
            } else {
                $data = ['upload_data' => $this->upload->data()];
                $post_image = $_FILES['userfile']['name'];
            }

            $this->Obat_model->save_obat_byid($post_image);

            //Set Message
            $this->session->set_flashdata(
                'success',
                'Obat has been Updated Successfull.'
            );

            $id = $this->input->post('jenis');
            if ($id == '1') {
                redirect('administrator/stok/obat/page/1');
            } else if ($id == '2') {
                redirect('administrator/stok/obat/page/2');
            } else {
                redirect('administrator/stok/obat/page/3');
            }
        }
    }

    public function obat_tambah($page = 'stok/add-obat')
    {

        // Check login
        if (!$this->session->userdata('login')) {
            redirect('administrator/index');
        }

        $data['title'] = 'Create User';

        //$data['add-user'] = $this->Administrator_Model->get_categories();

        $this->form_validation->set_rules('nama', 'Nama Obat', 'required');
        $this->form_validation->set_rules(
            'sku',
            'SKU',
            'required'
        );
        $css['css'] = 'add_obat';
        if ($this->form_validation->run() === false) {
            $this->load->view('administrator/header-script');
            $this->load->view('administrator/header');
            $this->load->view('administrator/header-bottom', $css);
            $this->load->view('administrator/' . $page, $data);
            $this->load->view('administrator/footer');
        } else {
            //Upload Image
            $config['upload_path'] = FCPATH . './assets/images/products/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['overwrite'] = true;
            $config['max_size'] = '10048';
            $config['max_width'] = '2000';
            $config['max_height'] = '2000';

            $this->load->library('upload', $config);

            $file_name = str_replace(' ', '_', $_FILES['userfile']['name']);

            if ($this->upload->do_upload('userfile')) {
                $post_image = $file_name;
            } else {
                $data['error'] = $this->upload->display_errors();
                $post_image = 'noimage.jpg';
            };
            $this->Obat_model->obat_tambah($post_image);

            //Set Message
            $this->session->set_flashdata(
                'success',
                'Obat has been created Successfull.'
            );

            $id = $this->input->post('jenis');
            if ($id == '1') {
                redirect('administrator/stok/obat/page/1');
            } else if ($id == '2') {
                redirect('administrator/stok/obat/page/2');
            } else {
                redirect('administrator/stok/obat/page/3');
            }
        }
    }







    public function add_stok($page = 'stok', $offset = 0)
    {
        if (!file_exists(APPPATH . 'views/administrator/stok/' . $page . '.php')) {
            show_404();
        }
        // Check login
        if (!$this->session->userdata('login')) {
            redirect('administrator/index');
        }

        //Data Stok
        // Pagination Config
        $config['base_url'] = base_url() . 'administrator/stok/';
        $config['total_rows'] = $this->db->count_all('tbl_stok');
        $config['per_page'] = 10;
        $config['uri_segment'] = 10;
        $config['attributes'] = ['class' => 'paginate-link'];

        // Init Pagination
        $this->pagination->initialize($config);

        $data['title'] = 'Stok';

        $data['rows'] = $this->Stok_model->get_stok(
            false,
            $config['per_page'],
            $offset
        );
        $data['rowsjenis'] = $this->Stok_model->get_jenis_stok();

        $data['title'] = 'Add Stok';

        //$data['add-user'] = $this->Administrator_Model->get_categories();

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('sku', 'SKU', 'required');
        $this->form_validation->set_rules(
            'jenis',
            'Jenis',
            'required'
        );
        $this->form_validation->set_rules(
            'deskripsi',
            'Deskripsi',
            'required'
        );
        $this->form_validation->set_rules(
            'jenis',
            'Jenis',
            'required'
        );
        $this->form_validation->set_rules(
            'stok',
            'Stok',
            'required'
        );
        $this->form_validation->set_rules(
            'biaya',
            'Biaya',
            'required'
        );
        $this->form_validation->set_rules(
            'tanggal_masuk',
            'Tanggal Masuk',
            'required'
        );
        $css['css'] = 'stok';
        if ($this->form_validation->run() === false) {
            $this->load->view('administrator/header-script');
            $this->load->view('administrator/header');
            $this->load->view('administrator/header-bottom', $css);
            $this->load->view('administrator/stok/' . $page, $data);
            $this->load->view('administrator/footer');
        } else {
            //Upload Image
            $config['upload_path'] = FCPATH . './assets/images/products/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['overwrite'] = true;
            $config['max_size'] = '10048';
            $config['max_width'] = '2000';
            $config['max_height'] = '2000';

            $this->load->library('upload', $config);

            $file_name = str_replace(' ', '_', $_FILES['fotostok']['name']);

            if ($this->upload->do_upload('fotostok')) {
                $post_image = $file_name;
            } else {
                $data['error'] = $this->upload->display_errors();
                $post_image = 'noimage.jpg';
            }
            $this->Stok_model->add_stok($post_image);

            //Set Message
            $this->session->set_flashdata(
                'success',
                'User has been created Successfull.'
            );
            redirect('administrator/stok');
        }
    }

    public function add_stok_unit()
    {

        $css['css'] = 'stok';
        $data = json_decode(file_get_contents('php://input'), true);
        $res = $this->Stok_model->add_stok_unit($data);

        if ($res) {
            echo base_url() . 'administrator/stok';
        } else {
            echo 0;
        }
    }

    public function get_stok_grup()
    {
        $data = $this->Stok_model->get_stok_grup();
        echo json_encode($data);
    }

    public function get_stok_grup_byid()
    {
        //BUTUH ID
        $data = json_decode(file_get_contents('php://input'), true);

        $data = $this->Stok_model->get_stok_grup_byid($data['sku']);
        if ($data) {
            echo json_encode($data);
        } else {
            echo false;
        }
    }

    public function update_total_stok()
    {
        //BUTUH ID
        $data = json_decode(file_get_contents('php://input'), true);

        $sku = $data['sku'];
        $stoklama = (int)$data['stoklama'];
        $stokbaru = (int)$data['stokbaru'];
        $jenis = $data['jenistransaksi'];

        (int)$stok = 0;
        if ($jenis == '0') {
            $stok = $stokbaru + $stoklama;
        } else {
            $stok = $stoklama - $stokbaru;
        }

        $res = $this->Stok_model->update_total_stok($sku, $stok);
        echo json_encode($res);
    }

    public function edit_riwayat_stok($id = null)
    {
        $data['row'] = $this->Stok_model->get_riwayat_stok_byid($id);

        if (empty($data['row'])) {
            show_404();
        }
        $data['title'] = 'Update Riwayat Stok';
        $css['css'] = 'stok';

        $this->load->view('administrator/header-script');
        $this->load->view('administrator/header');
        $this->load->view('administrator/header-bottom', $css);
        $this->load->view('administrator/stok/edit-riwayat-stok', $data);
        $this->load->view('administrator/footer');
    }

    public function update_riwayat_stok()
    {
        //BUTUH ID
        $data = json_decode(file_get_contents('php://input'), true);

        $stok = $data['stok'];
        $id = $data['id_riwayat_stok'];

        $res = $this->Stok_model->update_riwayat_stok($stok, $id);
        if ($res) {
            echo base_url() . 'administrator/stok';
        } else {
            echo 0;
        }
    }
}

/* End of file Stok_Controller.php and path /application/controllers/Admin/Stok_Controller.php */