<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Rekammedis_Controller extends CI_Controller
{
    var $useModel = "Rekammedis";
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/' . $this->useModel . '_model');
    }
    public function kunjungan()
    {
        // Pagination Config
        $config['base_url'] = base_url() . 'administrator/rekam-medis/kunjungan';

        // HITUNG TOTAL STOK DENGAN ID = 1 = OBAT;
        $config['total_rows'] = $this->db->count_all('tbl_rekam_medis');;
        $config['per_page'] = 10;
        $config['uri_segment'] = 10;
        $config['attributes'] = ['class' => 'paginate-link'];

        // Init Pagination
        $this->pagination->initialize($config);

        $data['title'] = 'Rekam Medis Pasien';

        $data['kunjungan'] = $this->Rekammedis_model->data_kunjungan();

        $data['dokter'] = $this->Rekammedis_model->data_dokter();

        $data['pasien'] = $this->Rekammedis_model->data_pasien();

        $this->form_validation->set_rules('pasien', 'Nama Pasien', 'required|trim');
        $this->form_validation->set_rules('dokter', 'Nama Dokter', 'required|trim');
        $this->form_validation->set_rules('tgl_kunjungan', 'Tanggal Kunjungan', 'required|trim');

        $css['css'] = "x";

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('administrator/header-script');
            $this->load->view('administrator/header');
            $this->load->view('administrator/header-bottom', $css);
            $this->load->view('administrator/rekam-medis/kunjungan', $data);
            $this->load->view('administrator/footer');
        } else {

            //Simpan ke database
            $this->Rekammedis_model->kunjungan_add();

            //Set Message
            $this->session->set_flashdata(
                'success',
                'Kunjungan has been created Successfull.'
            );
            redirect('administrator/rekam-medis/kunjungan');
        }
    }

    public function tambahjadwal()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $res = $this->Rekammedis_model->tambah_jadwal($data);
        if ($res) {
            echo base_url() . 'users/dashboard';
        } else {
            echo 0;
        }
    }

    public function kunjungan_byid($id)
    {
        // AMBIL DATA REKAM BY ID REKAM MEDIS
        $data['datarekam'] = $this->Rekammedis_model->data_kunjungan_byid($id);

        // AMBIL DATA RIWAYAT KUNJUNGAN
        $idPasien = $data['datarekam']['id_pasien'];
        $data['riwayatkunjungan'] = $this->Rekammedis_model->riwayat_kunjungan_byidpasien($idPasien);

        // AMBIL DATA RIWAYAT TRANSAKSI
        $data['riwayattransaksi'] = $this->Rekammedis_model->riwayat_transaksi_byidpasien($idPasien);


        // AMBIL DATA OBAT
        $data['obats'] = $this->Rekammedis_model->getobat();
        $data['resepobats'] = $this->Rekammedis_model->resepobat($id);

        // AMBIL DATA PERAWATAN
        $data['perawatan'] = $this->Rekammedis_model->getperawatan();
        // $data['resepperawatan'] = $this->Rekammedis_model->resepperawatan($id);

        $css['css'] = 'x';

        $this->load->view('administrator/header-script');
        $this->load->view('administrator/header');
        $this->load->view('administrator/header-bottom', $css);
        $this->load->view('administrator/rekam-medis/rekam', $data);
        $this->load->view('administrator/footer');
    }

    public function rekam_update()
    {

        // Check login
        if (!$this->session->userdata('login')) {
            redirect('administrator/index');
        }

        $data['title'] = 'Update Rekam';

        $res = $this->Rekammedis_model->rekam_update();

        $id =  $this->input->post('id_rekam');

        if (!$res) {
            //Set Message
            $this->session->set_flashdata(
                'error',
                'Data cannotd Updated.'
            );
            redirect('administrator/rekam-medis/kunjungan/' . $id);
        } else {
            //Set Message
            $this->session->set_flashdata(
                'success',
                'Data has been Updated Successfull.'
            );
            redirect('administrator/rekam-medis/kunjungan/' . $id);
        }
    }

    public function transaksi_add()
    {

        // Check login
        if (!$this->session->userdata('login')) {
            redirect('administrator/index');
        }
        $idstok = $this->input->post('id_stok');

        $res['data'] = $this->Rekammedis_model->get_stok_select($idstok);

        $biaya = $res['data']['biaya'];


        $res = $this->Rekammedis_model->transaksi_add($biaya);
        $id =  $this->input->post('id_rekam_medis');

        if (!$res) {
            //Set Message
            $this->session->set_flashdata(
                'error',
                'Data cannotd Updated.'
            );
            redirect('administrator/rekam-medis/kunjungan/' . $id);
        } else {
            //Set Message
            $this->session->set_flashdata(
                'success',
                'Data has been Updated Successfull.'
            );
            redirect('administrator/rekam-medis/kunjungan/' . $id);
        }
    }

    public function transaksi_hapus($idtransaksi, $idrekam)
    {

        // Check login
        if (!$this->session->userdata('login')) {
            redirect('administrator/index');
        }

        $res = $this->Rekammedis_model->transaksi_hapus($idtransaksi);


        if (!$res) {
            //Set Message
            $this->session->set_flashdata(
                'error',
                'Data cannotd Updated.'
            );
            redirect('administrator/rekam-medis/kunjungan/' . $idrekam);
        } else {
            //Set Message
            $this->session->set_flashdata(
                'success',
                'Data has been Updated Successfull.'
            );
            redirect('administrator/rekam-medis/kunjungan/' . $idrekam);
        }
    }
}