<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi_Controller extends CI_Controller
{

    var $useModel = "Transaksi";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/' . $this->useModel . '_model');
    }

    // CONTROLLER VIEW DAN DATA OBAT
    public function list()
    {
        // Pagination Config
        $config['base_url'] = base_url() . 'administrator/transaksi/list';

        // HITUNG TOTAL STOK DENGAN ID = 1 = OBAT;
        $config['total_rows'] = $this->Transaksi_model->get_total_transaksi();
        $config['per_page'] = 10;
        $config['uri_segment'] = 10;
        $config['attributes'] = ['class' => 'paginate-link'];

        // Init Pagination
        $this->pagination->initialize($config);

        $data['rows'] = $this->Transaksi_model->get_transaksi();

        $css['css'] = "x";
        $this->load->view('administrator/header-script');
        $this->load->view('administrator/header');
        $this->load->view('administrator/header-bottom', $css);
        $this->load->view('administrator/transaksi/transaksi', $data);
        $this->load->view('administrator/footer');
    }

    // ENABLE OBAT
    public function enable($id)
    {
        $table = base64_decode($this->input->get('table'));
        //$table = $this->input->post('table');
        $this->Transaksi_model->enable($id, $table);
        $this->session->set_flashdata('success', 'Desabled Successfully.');
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}

/* End of file Stok_Controller.php and path /application/controllers/Admin/Stok_Controller.php */