<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Stok_Controller extends CI_Controller
{

    var $useModel = "Stok";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/' . $this->useModel . '_model');
    }
    public function stok($offset = 0)
    {
        // Pagination Config
        $config['base_url'] = base_url() . 'administrator/stok/';
        $config['total_rows'] = $this->db->count_all('tbl_stok');
        $config['per_page'] = 10;
        $config['uri_segment'] = 10;
        $config['attributes'] = ['class' => 'paginate-link'];

        // Init Pagination
        $this->pagination->initialize($config);

        $data['title'] = 'Stok';

        $data['rows'] = $this->Stok_model->get_stok(
            false,
            $config['per_page'],
            $offset
        );
        $data['rowsjenis'] = $this->Stok_model->get_jenis_stok();
        $data['stok_grup'] = $this->Stok_model->get_stok_grup();


        $css['css'] = 'stok';
        $this->load->view('administrator/header-script');
        $this->load->view('administrator/header');
        $this->load->view('administrator/header-bottom', $css);
        $this->load->view('administrator/stok/stok', $data);
        $this->load->view('administrator/footer');
    }

    public function add_stok($page = 'stok', $offset = 0)
    {
        if (!file_exists(APPPATH . 'views/administrator/stok/' . $page . '.php')) {
            show_404();
        }
        // Check login
        if (!$this->session->userdata('login')) {
            redirect('administrator/index');
        }

        //Data Stok
        // Pagination Config
        $config['base_url'] = base_url() . 'administrator/stok/';
        $config['total_rows'] = $this->db->count_all('tbl_stok');
        $config['per_page'] = 10;
        $config['uri_segment'] = 10;
        $config['attributes'] = ['class' => 'paginate-link'];

        // Init Pagination
        $this->pagination->initialize($config);

        $data['title'] = 'Stok';

        $data['rows'] = $this->Stok_model->get_stok(
            false,
            $config['per_page'],
            $offset
        );
        $data['rowsjenis'] = $this->Stok_model->get_jenis_stok();

        $data['title'] = 'Add Stok';

        //$data['add-user'] = $this->Administrator_Model->get_categories();

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('sku', 'SKU', 'required');
        $this->form_validation->set_rules(
            'jenis',
            'Jenis',
            'required'
        );
        $this->form_validation->set_rules(
            'deskripsi',
            'Deskripsi',
            'required'
        );
        $this->form_validation->set_rules(
            'jenis',
            'Jenis',
            'required'
        );
        $this->form_validation->set_rules(
            'stok',
            'Stok',
            'required'
        );
        $this->form_validation->set_rules(
            'biaya',
            'Biaya',
            'required'
        );
        $this->form_validation->set_rules(
            'tanggal_masuk',
            'Tanggal Masuk',
            'required'
        );
        $css['css'] = 'stok';
        if ($this->form_validation->run() === false) {
            $this->load->view('administrator/header-script');
            $this->load->view('administrator/header');
            $this->load->view('administrator/header-bottom', $css);
            $this->load->view('administrator/stok/' . $page, $data);
            $this->load->view('administrator/footer');
        } else {
            //Upload Image
            $config['upload_path'] = FCPATH . './assets/images/products/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['overwrite'] = true;
            $config['max_size'] = '10048';
            $config['max_width'] = '2000';
            $config['max_height'] = '2000';

            $this->load->library('upload', $config);

            $file_name = str_replace(' ', '_', $_FILES['fotostok']['name']);

            if ($this->upload->do_upload('fotostok')) {
                $post_image = $file_name;
            } else {
                $data['error'] = $this->upload->display_errors();
                $post_image = 'noimage.jpg';
            }
            $this->Stok_model->add_stok($post_image);

            //Set Message
            $this->session->set_flashdata(
                'success',
                'User has been created Successfull.'
            );
            redirect('administrator/stok');
        }
    }

    public function add_stok_unit()
    {
        $css['css'] = 'stok';
        $data = json_decode(file_get_contents('php://input'), true);
        $res = $this->Stok_model->add_stok_unit($data);

        if ($res) {
            echo base_url() . 'administrator/stok';
        } else {
            echo 0;
        }
    }

    public function get_stok_grup()
    {
        $data = $this->Stok_model->get_stok_grup();
        echo json_encode($data);
    }

    public function get_stok_grup_byid()
    {
        //BUTUH ID
        $data = json_decode(file_get_contents('php://input'), true);

        $data = $this->Stok_model->get_stok_grup_byid($data['sku']);
        if ($data) {
            echo json_encode($data);
        } else {
            echo false;
        }
    }

    public function get_stok_select()
    {
        //BUTUH ID
        $data = json_decode(file_get_contents('php://input'), true);

        $data = $this->Stok_model->get_stok_select($data['sku']);
        if ($data) {
            echo json_encode($data);
        } else {
            echo false;
        }
    }

    public function update_total_stok()
    {
        //BUTUH ID
        $data = json_decode(file_get_contents('php://input'), true);

        $sku = $data['sku'];
        $stoklama = (int)$data['stoklama'];
        $stokbaru = (int)$data['stokbaru'];
        $jenis = $data['jenistransaksi'];

        (int)$stok = 0;
        if ($jenis == '0') {
            $stok = $stokbaru + $stoklama;
        } else {
            $stok = $stoklama - $stokbaru;
        }

        $res = $this->Stok_model->update_total_stok($sku, $stok);
        echo json_encode($res);
    }

    public function edit_riwayat_stok($id = null)
    {
        $data['row'] = $this->Stok_model->get_riwayat_stok_byid($id);

        if (empty($data['row'])) {
            show_404();
        }
        $data['title'] = 'Update Riwayat Stok';
        $css['css'] = 'stok';

        $this->load->view('administrator/header-script');
        $this->load->view('administrator/header');
        $this->load->view('administrator/header-bottom', $css);
        $this->load->view('administrator/stok/edit-riwayat-stok', $data);
        $this->load->view('administrator/footer');
    }

    public function update_riwayat_stok()
    {
        //BUTUH ID
        $data = json_decode(file_get_contents('php://input'), true);

        $stok = $data['stok'];
        $id = $data['id_riwayat_stok'];

        $res = $this->Stok_model->update_riwayat_stok($stok, $id);
        if ($res) {
            echo base_url() . 'administrator/stok';
        } else {
            echo 0;
        }
    }
}

/* End of file Stok_Controller.php and path /application/controllers/Admin/Stok_Controller.php */