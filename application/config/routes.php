<?php
defined('BASEPATH') or exit('No direct script access allowed');

//user routes
$route['users/register'] = 'users/register';
$route['users/dashboard'] = 'users/dashboard';
$route['comments/create/(:any)'] = 'comments/create/$1';
$route['categories'] = 'category/index';
$route['categories/create'] = 'category/create';
$route['categories/posts/(:any)'] = 'category/posts/$1';
$route['categories/delete/(:any)'] = 'category/delete/$1';
$route['posts/index'] = 'posts/index';
$route['posts/update'] = 'posts/update';
$route['posts/delete/(:any)'] = 'posts/delete/$1';
$route['posts/create'] = 'posts/create';
$route['posts/(:any)'] = 'posts/view/$1';
$route['posts'] = 'posts/index';
$route['default_controller'] = 'pages/view';

// ===========
///admin routs
$route['administrator'] = 'administrator/view';
$route['administrator/home'] = 'administrator/home';
$route['administrator/index'] = 'administrator/view';
$route['administrator/forget-password'] = 'administrator/forget_password';

$route['administrator/dashboard'] = 'administrator/dashboard';

$route['administrator/change-password'] = 'administrator/get_admin_data';
$route['administrator/update-profile'] = 'administrator/update_admin_profile';

// Routers users
$route['administrator/users'] = 'Admin/Users_Controller/index';
$route['administrator/users/add-user'] = 'Admin/Users_Controller/add_user';
$route['administrator/users/enable/(:any)'] = 'Admin/Users_Controller/enable/$1';
$route['administrator/users/disable/(:any)'] = 'Admin/Users_Controller/disable/$1';
$route['administrator/users/delete/(:any)'] = 'Admin/Users_Controller/delete/$1';
$route['administrator/users/update-user/(:any)'] =
    'Admin/Users_Controller/update_user/$1';

// Routers Perawatan
$route['administrator/perawatan'] = 'Admin/Perawatan_Controller/index';
$route['administrator/perawatan/add-perawatan'] = 'Admin/Perawatan_Controller/add_perawatan';
$route['administrator/perawatan/enable/(:any)'] = 'Admin/Perawatan_Controller/enablePerawatan/$1';
$route['administrator/perawatan/disable/(:any)'] = 'Admin/Perawatan_Controller/disablePerawatan/$1';
$route['administrator/perawatan/delete/(:any)'] = 'Admin/Perawatan_Controller/deletePerawatan/$1';
$route['administrator/perawatan/update-perawatan/(:any)'] =
    'Admin/Perawatan_Controller/update_perawatan/$1';
$route['administrator/perawatan/do-update-perawatan'] =
    'Admin/Perawatan_Controller/update_perawatan_data';

// Routers Jenis Perawatan
$route['administrator/perawatan/jenis'] = 'Admin/Perawatan_Controller/jenis_perawatan';
$route['administrator/perawatan/add-jenis-perawatan'] = 'Admin/Perawatan_Controller/add_jenis_perawatan';
$route['administrator/perawatan/update-jenis-perawatan/(:any)'] =
    'Admin/Perawatan_Controller/update_jenis_perawatan/$1';
$route['administrator/perawatan/do-update-jenis-perawatan'] =
    'Admin/Perawatan_Controller/update_jenis_perawatan_data';
$route['administrator/perawatan/delete_jenis_perawatan/(:any)'] = 'Admin/Perawatan_Controller/delete_jenis_perawatan/$1';

// Routers Kelola Stok
$route['administrator/stok'] = 'Admin/Stok_Controller/stok';
$route['administrator/stok/add-stok'] = 'Admin/Stok_Controller/add_stok';
$route['administrator/stok/add-stok-unit'] = 'Admin/Stok_Controller/add_stok_unit';
$route['administrator/stok/grup'] = 'Admin/Stok_Controller/get_stok_grup';
$route['administrator/stok/select'] = 'Admin/Stok_Controller/get_stok_select';
$route['administrator/stok/update-total-stok'] = 'Admin/Stok_Controller/update_total_stok';
$route['administrator/stok/edit-riwayat-stok/(:any)'] =
    'Admin/Stok_Controller/edit_riwayat_stok/$1';
$route['administrator/stok/update-riwayat-stok'] = 'Admin/Stok_Controller/update_riwayat_stok';

// Router Kelola Stok - Obat
$route['administrator/stok/obat/page/(:any)'] = 'Admin/Obat_Controller/obat/$1';
$route['administrator/stok/obat/enable/(:any)'] = 'Admin/Obat_Controller/enable/$1';
$route['administrator/stok/obat/disable/(:any)'] = 'Admin/Obat_Controller/disable/$1';
$route['administrator/stok/obat/edit-obat/(:any)'] =
    'Admin/Obat_Controller/edit_obat/$1';
$route['administrator/stok/obat-edit/save-edit-obat'] =
    'Admin/Obat_Controller/save_obat_byid';
$route['administrator/stok/obat-tambah'] = 'Admin/Obat_Controller/obat_tambah';


// Router Rekam Medis
$route['administrator/rekam-medis/kunjungan'] = 'Admin/Rekammedis_Controller/kunjungan';
$route['administrator/rekam-medis/kunjungan/(:any)'] = 'Admin/Rekammedis_Controller/kunjungan_byid/$1';
$route['administrator/rekam-medis/kunjungan-add/add'] = 'Admin/Rekammedis_Controller/kunjungan';
$route['administrator/rekam-medis/kunjungan-jadwal'] = 'Admin/Rekammedis_Controller/tambahjadwal';
$route['administrator/rekam-medis/rekam-update'] = 'Admin/Rekammedis_Controller/rekam_update';
$route['administrator/rekam-medis/transaksi-add'] = 'Admin/Rekammedis_Controller/transaksi_add';
$route['administrator/rekam-medis/transaksi-hapus/(:any)/(:any)'] = 'Admin/Rekammedis_Controller/transaksi_hapus/$1/$2';





$route['administrator/blogs/add-blog'] = 'administrator/add_blog';
$route['administrator/blogs/list-blog'] = 'administrator/list_blog';
$route['administrator/blogs/update-blog'] = 'administrator/update_blog';

$route['administrator/product-categories/create'] =
    'administrator/create_product_category';
$route['administrator/product-categories/update/(:any)'] =
    'administrator/update_product_category/$1';
$route['administrator/product-categories'] = 'administrator/product_categories';
//$route['administrator/product-categories/(:any)'] = 'administrator/update_product_category/$1';

$route['administrator/products/create'] = 'administrator/create_product';
$route['administrator/products'] = 'administrator/get_products';
$route['administrator/products/update/(:any)'] =
    'administrator/update_products/$1';

$route['administrator/faq-categories/create'] =
    'administrator/create_faq_category';
$route['administrator/faq-categories/update/(:any)'] =
    'administrator/update_faq_category/$1';
$route['administrator/faq-categories'] = 'administrator/faq_categories';

$route['administrator/faq/create'] = 'administrator/create_faq';
$route['administrator/faqs'] = 'administrator/get_faqs';
$route['administrator/faqs/update/(:any)'] = 'administrator/update_faqs/$1';

$route['administrator/scopages'] = 'administrator/get_scopages';
$route['administrator/sco-pages/update/(:any)'] =
    'administrator/update_scopages/$1';

$route['administrator/sociallinks'] = 'administrator/get_sociallinks';
$route['administrator/sociallinks/update/(:any)'] =
    'administrator/update_sociallinks/$1';

$route['administrator/sliders/create'] = 'administrator/create_slider';
$route['administrator/sliders'] = 'administrator/get_sliders';
$route['administrator/sliders/update/(:any)'] =
    'administrator/update_slider/$1';

$route['administrator/site-configuration'] =
    'administrator/get_siteconfiguration';
$route['administrator/site-configuration/update/(:any)'] =
    'administrator/update_siteconfiguration/$1';

$route['administrator/page-contents'] = 'administrator/get_pagecontents';
$route['administrator/page-contents/update/(:any)'] =
    'administrator/update_pagecontents/$1';

$route['administrator/galleries/add'] = 'galleries/galleriesLoad';
$route['administrator/galleries'] = 'galleries/get_gallery_images';

$route['administrator/blogs/blog-comments'] =
    'administrator/list_blog_comments';
$route['administrator/blogs/view-comment/(:any)'] =
    'administrator/view_blog_comments/$1';

$route['administrator/team/add'] = 'administrator/add_team';
$route['administrator/team/list'] = 'administrator/list_team';
$route['administrator/team/update/(:any)'] = 'administrator/update_team/(:any)';

$route['administrator/testimonials/add'] = 'administrator/add_testimonial';
$route['administrator/testimonials/list'] = 'administrator/list_testimonial';
$route['administrator/testimonials/update/(:any)'] =
    'administrator/update_testimonial/(:any)';


//Transaksi
$route['administrator/transaksi/list'] = 'Admin/Transaksi_Controller/list';
$route['administrator/transaksi/list/enable/(:any)'] = 'Admin/Transaksi_Controller/enable/$1';

//HOME
$route['produk/(:any)'] =
    'Pages/produk/$1';

$route['home/cart/add'] = 'cart/add_to_cart';
$route['administrator/accesslog/upload'] = 'administrator/uploadaccesslog';


$route['(:any)'] = 'pages/view/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = false;