<?php
class Cart_model extends CI_Model
{

    function get_all_produk()
    {
        $hasil = $this->db->get('tbl_stok');
        return $hasil->result();
    }

    function save_transaksi($datas)
    {
        $kodetrans = 'TRS' . $datas['iduser'] . rand(10, 999);
        foreach ($datas['data'] as $key => $value) {
            $data = array(
                'kode_transaksi' => $kodetrans,
                'nama_penerima' => $datas['nama'],
                'alamat_penerima' => $datas['alamat'],
                'hp_penerima' => $datas['hp'],
                'id_user' => $datas['iduser'],
                'id_stok' => $value['id'],
                'jumlah' => $value['qty'],
                'total' => $value['price'],

            );
            $this->db->insert('tbl_transaksi', $data);
        }
        return true;
    }
}