<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home_model extends CI_Model
{
    public function kategorilayanan()
    {
        $query_str = "SELECT *
        FROM tbl_jenis_perawatan
        ORDER BY update_at DESC";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function getAllProduct()
    {
        $this->db->select('*');
        $this->db->from('tbl_stok');
        $this->db->order_by('update_at', 'DESC');

        return $this->db->get();
    }

    public function getDataPagination($limit, $offset)
    {
        $this->db->select('*');
        $this->db->from('tbl_stok');
        $this->db->order_by('update_at', 'DESC');
        $this->db->limit($limit, $offset);

        return $this->db->get();
    }


    public function produk($id = FALSE)
    {
        $query_str = "SELECT tbl_stok.* , tbl_kategori_stok.nama as kategori, tbl_kategori_stok.deskripsi as deskripsikategori
        FROM tbl_stok
        JOIN tbl_kategori_stok on tbl_kategori_stok.id_kategori_stok = tbl_stok.id_jenis
        WHERE id_stok = $id ";
        $query = $this->db->query($query_str);
        return $query->row_array();
    }

    public function detailtransaksi($id)
    {
        $query_str = "SELECT tbl_transaksi.* , tbl_stok.nama, tbl_stok.sku, tbl_stok.nama as kategori 
        FROM tbl_transaksi
        JOIN tbl_stok on tbl_stok.id_stok = tbl_transaksi.id_stok
        JOIN tbl_kategori_stok on tbl_stok.id_jenis = tbl_kategori_stok.id_kategori_stok
        WHERE kode_transaksi = '$id' ";
        $query = $this->db->query($query_str);
        return $query->row_array();
    }

    public function totalbayar($id)
    {
        $query_str = "SELECT SUM(total) as totalbayar
        FROM tbl_transaksi
        WHERE kode_transaksi = '$id' 
        GROUP BY kode_transaksi ";
        $query = $this->db->query($query_str);
        return $query->row_array();
    }

    public function updatetransaksi($post_image)
    {
        $data = array(
            'pengirim' => $this->input->post('namapengirim'),
            'img' => $post_image,
            'status' => 1,

        );
        $this->db->where('kode_transaksi', $this->input->post('id'));
        return $this->db->update('tbl_transaksi', $data);
    }
}


/* End of file Home_model.php and path /application/models/Home_model.php */