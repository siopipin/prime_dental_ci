<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi_model extends CI_Model
{
    public function get_total_transaksi()
    {
        $query_str = "SELECT COUNT(*) FROM tbl_transaksi";
        return $this->db->query($query_str);
    }


    public function get_transaksi()
    {
        $query_str = "SELECT tbl_transaksi.*, users.name, tbl_stok.nama as namastok
        FROM tbl_transaksi
        JOIN users on users.id = tbl_transaksi.id_user
        JOIN tbl_stok on tbl_transaksi.id_stok = tbl_stok.id_stok";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function enable($id, $table)
    {
        $data = array(
            'status' => 2
        );
        $this->db->where('kode_transaksi', $id);
        return $this->db->update($table, $data);
    }
}