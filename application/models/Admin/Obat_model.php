<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Obat_model extends CI_Model
{

    // AMBIL TOTAL OBAT
    public function get_total_obat($jenis)
    {
        $query_str = "SELECT COUNT(*) FROM tbl_stok WHERE id_jenis = $jenis";
        return $this->db->query($query_str);
    }

    // AMBIL DATA OBAT
    public function get_obat($jenis)
    {
        $query_str = "SELECT jenis.nama as jenis, stok.sku, stok.id_stok, stok.nama, stok.stok, stok.deskripsi, stok.satuan, stok.biaya, stok.tanggal_masuk, stok.status, stok.img
        FROM tbl_stok stok
        JOIN tbl_kategori_stok jenis ON jenis.id_kategori_stok = stok.id_jenis
        WHERE id_jenis = $jenis ORDER BY stok.tanggal_masuk DESC";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    // DISABLE OBAT
    public function enable($id, $table)
    {
        $data = array(
            'status' => 0
        );
        $this->db->where('id_stok', $id);
        return $this->db->update($table, $data);
    }

    // ENABLE OBAT
    public function disable($id, $table)
    {
        $data = array(
            'status' => 1
        );
        $this->db->where('id_stok', $id);
        return $this->db->update($table, $data);
    }

    public function get_obat_byid($id = FALSE)
    {
        $query = $this->db->get_where('tbl_stok', array('id_stok' => $id));
        return $query->row_array();
    }

    public function get_kategori()
    {
        $query_str = "SELECT * FROM tbl_kategori_stok ORDER BY update_at DESC";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function save_obat_byid($post_image)
    {
        $data = array(
            'nama' => $this->input->post('nama'),
            'sku' => $this->input->post('sku'),
            'id_jenis' => $this->input->post('jenis'),
            'deskripsi' => $this->input->post('deskripsi'),
            'biaya' => $this->input->post('biaya'),
            'satuan' => $this->input->post('satuan'),
            'img' => $post_image,
            'status' => $this->input->post('status'),
        );

        $this->db->where('id_stok', $this->input->post('id'));
        $d = $this->db->update('tbl_stok', $data);
    }

    public function obat_tambah($post_image)
    {
        $data = array(
            'nama' => $this->input->post('nama'),
            'sku' => $this->input->post('sku'),
            'id_jenis' => $this->input->post('jenis'),
            'deskripsi' => $this->input->post('deskripsi'),
            'biaya' => $this->input->post('biaya'),
            'satuan' => $this->input->post('satuan'),
            'img' => $post_image,
            'status' => $this->input->post('status'),

        );
        return $this->db->insert('tbl_stok', $data);
    }









    public function get_stok_grup_byid($sku)
    {
        $query_str = "SELECT tbl_riwayat_stok.stok, tbl_riwayat_stok.tgl_transaksi, tbl_stok.nama, 
        tbl_stok.stok, tbl_stok.img, tbl_stok.sku, tbl_stok.satuan, tbl_kategori_stok.nama as jenis, users.id, users.name, tbl_riwayat_stok.status, tbl_riwayat_stok.id_riwayat_stok, tbl_riwayat_stok.id_transaksi, tbl_riwayat_stok.updated_at, tbl_riwayat_stok.notes
        FROM tbl_riwayat_stok
        JOIN tbl_stok on tbl_stok.sku = tbl_riwayat_stok.sku
        JOIN tbl_kategori_stok on tbl_kategori_stok.id_kategori_stok = tbl_stok.id_jenis
        JOIN users on users.id = tbl_riwayat_stok.id_user
        WHERE tbl_stok.sku = '$sku' 
        ORDER BY tbl_riwayat_stok.updated_at DESC";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }



    public function get_stok_list()
    {
        $query_str = "SELECT * FROM tbl_stok ORDER BY update_at DESC";
        $query = $this->db->query($query_str);
        return $query->result();
    }

    public function add_stok($post_image)
    {
        $data = array(
            'nama' => $this->input->post('nama'),
            'sku' => $this->input->post('sku'),
            'id_jenis' => $this->input->post('jenis'),
            'deskripsi' => $this->input->post('deskripsi'),
            'stok' => $this->input->post('stok'),
            'satuan' => $this->input->post('satuan'),
            'tanggal_masuk' => $this->input->post('tanggal_masuk'),
            'status' => $this->input->post('status'),
            'img' => $post_image,
        );
        return $this->db->insert('tbl_stok', $data);
    }

    public function add_stok_unit($data)
    {
        $data = array(
            'sku' => $data['data']['sku'],
            'status' => $data['status'],
            'id_user' => $data['id_user'],
            'id_transaksi' => $data['id_transaksi'],
            'stok' => $data['stok'],
            'notes' => $data['notes'],
        );
        return $this->db->insert('tbl_riwayat_stok', $data);
    }

    public function update_total_stok($sku, $stok)
    {
        $query_str = "UPDATE tbl_stok SET stok=$stok WHERE sku= '$sku' ";
        $query = $this->db->query($query_str);
        return (bool) $query;
    }

    public function get_riwayat_stok_byid($id = FALSE)
    {
        $query_str = "SELECT tbl_riwayat_stok.id_riwayat_stok, tbl_stok.nama, tbl_riwayat_stok.stok
        FROM tbl_riwayat_stok 
        JOIN tbl_stok on tbl_stok.sku = tbl_riwayat_stok.sku
        WHERE id_riwayat_stok = $id";
        $query = $this->db->query($query_str);
        return $query->row_array();
    }

    public function update_riwayat_stok($stok, $id)
    {
        $query_str = "UPDATE tbl_riwayat_stok SET stok=$stok WHERE id_riwayat_stok= '$id' ";
        $query = $this->db->query($query_str);
        return (bool) $query;
    }
}


/* End of file Stok_model.php and path /application/models/Admin/Stok_model.php */