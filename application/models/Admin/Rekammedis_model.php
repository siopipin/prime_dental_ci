<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Rekammedis_model extends CI_Model
{
    public function data_kunjungan()
    {
        $query_str = "SELECT tbl_rekam_medis.id_rekam_medis, tbl_rekam_medis.tgl_kunjungan, tbl_rekam_medis.keluhan, tbl_rekam_medis.penatalaksaan, a.name, b.name as nama_dokter, tbl_rekam_medis.status
        FROM tbl_rekam_medis
        LEFT JOIN users a on a.id = tbl_rekam_medis.id_pasien
        LEFT JOIN users b on b.id = tbl_rekam_medis.id_dokter";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function tambah_jadwal($data)
    {
        $datas = array(
            'id_pasien' => $data['idpasien'],
            'nama_pasien' => $data['nama'],
            'tgl_kunjungan' => $data['tgl'],
            'keluhan' => $data['keluhan']
        );
        return $this->db->insert('tbl_rekam_medis', $datas);
    }


    public function data_kunjungan_byid($id)
    {
        $query_str = "SELECT tbl_rekam_medis.id_rekam_medis, tbl_rekam_medis.tgl_kunjungan, tbl_rekam_medis.keluhan, tbl_rekam_medis.penatalaksaan,tbl_rekam_medis.id_pasien, a.name, a.gender, a.dob, b.name as nama_dokter, tbl_rekam_medis.id_perawatan, tbl_rekam_medis.status
        FROM tbl_rekam_medis
        LEFT JOIN users a on a.id = tbl_rekam_medis.id_pasien
        LEFT JOIN users b on b.id = tbl_rekam_medis.id_dokter
        WHERE tbl_rekam_medis.id_rekam_medis = $id";
        $query = $this->db->query($query_str);
        return $query->row_array();
    }

    public function riwayat_kunjungan_byidpasien($idpasien)
    {
        $query_str = "SELECT tbl_rekam_medis.id_rekam_medis, tbl_rekam_medis.tgl_kunjungan, tbl_rekam_medis.keluhan, tbl_rekam_medis.penatalaksaan, tbl_rekam_medis.id_pasien, a.name, b.name as nama_dokter, tbl_rekam_medis.id_perawatan, tbl_rekam_medis.status
        FROM tbl_rekam_medis
        LEFT JOIN users a on a.id = tbl_rekam_medis.id_pasien
        LEFT JOIN users b on b.id = tbl_rekam_medis.id_dokter
        WHERE tbl_rekam_medis.id_pasien = $idpasien";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function riwayat_transaksi_byidpasien($idpasien)
    {
        $query_str = "SELECT tbl_transaksi.* , tbl_stok.nama, tbl_stok.satuan, tbl_kategori_stok.nama as kategori
        FROM tbl_transaksi 
        JOIN tbl_stok ON tbl_stok.id_stok = tbl_transaksi.id_stok
        JOIN users on users.id = tbl_transaksi.id_user
        JOIN tbl_kategori_stok on tbl_kategori_stok.id_kategori_stok = tbl_stok.id_jenis
        WHERE tbl_transaksi.id_user =$idpasien";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function grup_riwayat_transaksi_byidpasien($idpasien)
    {
        $query_str = "SELECT MAX(tgl_transaksi) as tgl_transaksi, tbl_transaksi.kode_transaksi, tbl_transaksi.status, tbl_transaksi.nama_penerima, tbl_transaksi.hp_penerima, COUNT(kode_transaksi) as total
        FROM tbl_transaksi 
        WHERE tbl_transaksi.id_user =$idpasien
        GROUP BY kode_transaksi, tbl_transaksi.status, nama_penerima, hp_penerima";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function data_pasien()
    {
        $query_str = "SELECT * FROM users WHERE role_id = 4";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function data_dokter()
    {
        $query_str = "SELECT * FROM users WHERE role_id = 3";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function kunjungan_add()
    {
        $data = array(
            'id_pasien' => $this->input->post('pasien'),
            'id_dokter' => $this->input->post('dokter'),
            'tgl_kunjungan' => $this->input->post('tgl_kunjungan'),
        );
        return $this->db->insert('tbl_rekam_medis', $data);
    }

    public function getobat()
    {
        $query_str = "SELECT * FROM tbl_stok WHERE id_jenis = 1";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function resepobat($idrekammedis)
    {
        $query_str = "SELECT tbl_transaksi.* , tbl_stok.nama, tbl_stok.satuan, tbl_stok.img
        FROM tbl_transaksi
        JOIN tbl_stok ON tbl_stok.id_stok = tbl_transaksi.id_stok
        WHERE id_rekam_medis = $idrekammedis";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function getperawatan()
    {
        $query_str = "SELECT * FROM tbl_perawatan";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function rekam_update()
    {
        $data = array(
            'keluhan' => $this->input->post('keluhan'),
            'id_perawatan' => $this->input->post('perawatan'),
            'penatalaksaan' => $this->input->post('penatalaksaan')
        );

        $this->db->where('id_rekam_medis', $this->input->post('id_rekam'));
        return $this->db->update('tbl_rekam_medis', $data);
    }

    public function transaksi_add($biaya)
    {

        $kode = $this->input->post('kode_transaksi');
        if ($kode == null) {
            $kode = 'TRS' . rand(10, 9999);
        } else {
            $kode = $this->input->post('kode_transaksi');
        }

        //HITUNG TOTAL
        $jlh = $this->input->post('jumlah');
        $ttl = $jlh * $biaya;

        $data = array(
            'kode_transaksi' => $kode,
            'id_user' => $this->input->post('id_user'),
            'id_rekam_medis' => $this->input->post('id_rekam_medis'),
            'id_stok' => $this->input->post('id_stok'),
            'jumlah' => $this->input->post('jumlah'),
            'total' => $ttl
        );
        return $this->db->insert('tbl_transaksi', $data);
    }

    public function get_stok_select($id)
    {
        $query_str = "SELECT *
        FROM tbl_stok
        WHERE tbl_stok.id_stok = '$id'";
        $query = $this->db->query($query_str);
        return $query->result_array();
    }

    public function transaksi_hapus($idtransaksi)
    {
        $this->db->where('id_transaksi', $idtransaksi);
        $this->db->delete('tbl_transaksi');
        return true;
    }
}