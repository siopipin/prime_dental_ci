<section id="home">

    <div class="container">

        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

            <div class="carousel-inner" role="listbox">
                <br><br><br>

                <div class="item active">
                    <div class="carousel-caption">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="block">
                                    <img class="app-img img-responsive"
                                        src="<?= base_url('assets/store/'); ?>images/slider/dental.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-1 col-sm-6">
                                <div class="block">
                                    <h1>
                                        Professional Dental Studio <br> since 2014 in Medan.
                                    </h1>
                                    <p>
                                        by drg. Fenny Valentine Gouw
                                    </p>
                                    <p>
                                        Atur jadwal dan segera periksa kesehatan mulut dan gigi
                                    </p>

                                    <ul class="download-btn">
                                        <?php if ($this->session->userdata('login')) : ?>
                                        <li>
                                            <button type="button" id="penjadwalan" class="btn btn-default btn-red"
                                                data-toggle="modal" data-target="#formmodal"><i
                                                    class="fa fa-book"></i>PENJADWALAN</button>
                                        </li>
                                        <?php endif; ?>
                                        <?php if (!$this->session->userdata('login')) : ?>
                                        <li>
                                            <a class="btn btn-default btn-red"
                                                href="<?php echo base_url(); ?>users/login">Login untuk
                                                buat jadwal</a>
                                        </li>
                                        <?php endif; ?>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="services">
    <div class="container">
        <h1 class="title">Layanan Kami</h1>
        <hr class="divider" style="width:50%;border-color:#CCC;">
        <p class="text-center">Layanan Perawatan Gigi dan Mulut Terbaik</p>
        <div class="service-wrapper">
            <div class="row">

                <?php foreach ($kategorilayanan as $kategori) : ?>
                <div class="col-md-3 col-sm-6">
                    <div class="block wow fadeInRight" data-wow-delay="1s">
                        <div class="icon">
                            <i class="<?= $kategori['icon']; ?>"></i>
                        </div>

                        <h3><?= $kategori['nama']; ?></h3>
                        <p><?= $kategori['deskripsi']; ?></p>
                    </div>
                </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</section>

<section id="products">
    <div class="container">
        <h1 class="title">Produk Banyak diakses</h1>
        <hr class="divider" style="width:50%;">
        <p class="text-center">Paling banyak dilihat</p>
        <div class="row" style="margin-top:40px;">
            <?php
            $dbjson = file_get_contents(FCPATH . './assets/logs/result.json');
            $produks = json_decode($dbjson);
            foreach ($all as $key => $row) { ?>

            <?php foreach ($produks as $i => $val) { ?>
            <?php if ($row->id_stok == $produks[$i]->produk) { ?>
            <div class="col-md-3">
                <div class="thumbnail">
                    <img style="width: 150px; height: 140px; object-fit: cover;"
                        src="<?php echo base_url() . 'assets/images/products/' . $row->img; ?>">
                    <div class="caption">
                        <h4><?php echo $row->nama; ?></h4>

                        <div class="row">
                            <div class="col-md-7">
                                <p><b><?php echo 'Rp ' . number_format($row->biaya); ?></b></p>
                            </div>

                        </div>
                        <a href="<?php echo base_url(); ?>produk/<?php echo $row->id_stok; ?>"
                            class="btn btn-default btn-grey btn-block"> <i class="fa fa-list"></i>

                            Detail</a>

                    </div>
                </div>
            </div>
            <?php } ?>
            <?php } ?>

            <?php } ?>



        </div>
    </div>
</section>

<section id="products">
    <div class="container">
        <h1 class="title">Products</h1>
        <hr class="divider" style="width:50%;">
        <p class="text-center">Daftar Produk</p>
        <div class="row" style="margin-top:40px;">
            <?php foreach ($allstok as $row) : ?>


            <div class="col-md-3">
                <div class="thumbnail">
                    <img style="width: 200px; height: 220px; object-fit: cover;"
                        src="<?php echo base_url() . 'assets/images/products/' . $row->img; ?>">
                    <div class="caption">
                        <h4><?php echo $row->nama; ?></h4>
                        <p><?= substr($row->deskripsi, 0, 20) ?>...</p>

                        <div class="row">
                            <div class="col-md-7">
                                <h4><?php echo 'Rp ' . number_format($row->biaya); ?></h4>
                            </div>

                        </div>
                        <a href="<?php echo base_url(); ?>produk/<?php echo $row->id_stok; ?>"
                            class="btn btn-default btn-grey btn-block"> <i class="fa fa-list"></i>

                            Detail</a>

                    </div>
                </div>
            </div>
            <?php endforeach; ?>


            <div class="text-center">

                <ul class="pagination pagination-lg wow fadeInUp" data-wow-delay="1.1s">
                    <?php echo $this->pagination->create_links(); ?>
                </ul>
            </div>


        </div>
    </div>
</section>

<section id="testimonials">
    <div id="carousel-example-generict" class="carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generict" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generict" data-slide-to="1"></li>
        </ol>


        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <div class="carousel-caption">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="block text-center">
                                <img class="img-responsive img-circle center-block"
                                    src="<?= base_url('assets/store/'); ?>images/testimonials/kelvin.jpeg" alt=""
                                    style="height:100px;">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 text-center">
                            <div class="block">
                                <h3 style="color:#ff0f37;" class="text-center">Absolutely Brilliant <br> drg. Kelvin
                                    Tantono - Medan</h3>
                                <p style="color:#111;font-size:14px;">
                                    Klinik gigi kami memiliki konsep modern dental clinics dengan bantuan teknologi
                                    dental radiografi (x-ray), <br> advanced dentistry, dan intraoral camera. Klinik
                                    kami
                                    memiliki suasana nyaman, <br> perawatan gigi lengkap dan dokter gigi yang ramah.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="item">
                <div class="carousel-caption">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="block text-center">
                                <img class="img-responsive img-circle center-block"
                                    src="<?= base_url('assets/store/'); ?>images/testimonials/drkev.png" alt=""
                                    style="height:100px;">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 text-center">
                            <div class="block">
                                <h3 style="color:#ff0f37;" class="text-center">Amazing Service! - drg. Rendy - Binjai
                                </h3>
                                <p style="color:#111;font-size:14px;">
                                    Dengan pelayanan terbaik untuk perawatan gigi dan mulut terbaik di Medan
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>
</section>

<section id="contact" class="wow fadeInUp bgc-one-top mts-section-wrapper mts-contact-section" data-wow-delay=".8s"
    style="margin-top:50px;margin-bottom:50px;">
    <div class="container">
        <div class="row">
            <h1 class="title">Hubungi Kami</h1>
            <hr class="divider" style="width:50%;">
            <p class="text-center">Ada pertanyaan terkait pemeriksaan gigi dan mulut? hubungi kami segera.</p>
            <!-- Section Header End -->

            <div class="mts-contact-details" style="margin-top:30px;">

                <!-- Address Area End -->

                <!-- Contact Form -->
                <div class="col-md-6 col-sm-6 col-xs-12 mts-contact-form wow bounceInRight">
                    <div id="contact-result"></div>
                    <div id="contact-form">
                        <div class="mts-input-name mts-input-fields">
                            <input type="text" name="name" id="name" placeholder="Name">
                        </div>

                        <div class="mts-input-email mts-input-fields">
                            <input type="email" name="email" id="email" placeholder="Email">
                        </div>

                        <div class="mts-input-message mts-input-fields">
                            <textarea name="message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
                        </div>

                        <input type="submit" value="SEND MESSAGE" id="submit-btn">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p>Pada masa pandemi saat ini, klinik gigi prime dental menerapkan prosedur protokol yang ketat,
                        salah satunya dengan menyediakan media digital untuk membuat perjanjian dan pemesanan layanan,
                        produk dan hal terkait perawatan gigi dan mulut.</p>
                    <ul>
                        <li><i class="fa fa-home"></i>ᴊʟ. ᴡᴜsʜᴜ ɴᴏ. 𝟸ᴅ/𝟾. - Medan, Indonesia</li>
                        <li><i class="fa fa-phone"></i>𝟎𝟔𝟏 𝟕𝟑𝟒𝟔 𝟏𝟖𝟕</li>
                        <li><i class="fa fa-instagram"></i>prime.dentalstudio</li>
                    </ul>
                </div>
                <!-- Contact Form End -->

            </div>
        </div>
    </div>
</section>

<footer class="wow fadeInUp" data-wow-delay=".8s">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <a class="footer-logo" href="#">

                </a>
                <p>Copyright © 2021 <a href="#">Prime Dental Studio</a></p>

            </div>
        </div>
    </div>
</footer>


<!-- Modal Unit Stok -->
<div class="modal fade" id="formmodal" tabindex="-1" aria-labelledby="formModalLabelDokter" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabelDokter">Buat Jadwal Kunjungan</h5>

            </div>
            <div class="modal-body">
                <div class="alert alert-warning alert-dismissible fade show" id="warning" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="form-group">
                    <label for="stok">Nama Pasien</label>

                    <input class="form-control" type="text" id="nama"
                        placeholder="Isi nama pasien yang butuh perawatan">

                    <small class="muted text-danger"><?= form_error(
                                                            'nama'
                                                        ) ?></small>
                </div>
                <div class="form-group">
                    <label for="stok">Keluhan</label>

                    <input class="form-control" type="text" id="keluhan" placeholder="Isi keluhan">

                    <small class="muted text-danger"><?= form_error(
                                                            'keluhan'
                                                        ) ?></small>
                </div>

                <div class="form-group">
                    <label for="stok">Request Waktu kunjungan</label>

                    <input class="form-control" type="date" id="tgl" id="notes">

                    <small class="muted text-danger"><?= form_error(
                                                            'date'
                                                        ) ?></small>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button name="submit" class="btn btn-primary" id="tambahpenjadwalan">Tambah</button>
                </div>
            </div>
        </div>
    </div>
</div>