<section id="home">
    <div class="container">
        <br><br><br><br><br>

        <div class="row">
            <div class="col-md-2">
                <div class="block">
                    <img style="width: 250px; height: 240px; object-fit: cover;" class="app-img img-responsive"
                        src="<?php echo base_url() .
                                                                                                                            'assets/images/products/' .
                                                                                                                            $produkdetail['img']; ?>" alt="">
                </div>
            </div>
            <div class="col-md-4 col-md-offset-1 col-sm-6">
                <div class="block">
                    <h1>
                        <?php echo $produkdetail['nama']; ?>
                    </h1>
                    <p>
                        <?php echo $produkdetail['deskripsi']; ?>

                    </p>


                </div>
            </div>

            <div class="col-md-4 col-md-offset-1 col-sm-6">
                <div class="card" style="width: 18rem;">

                    <div class="card-body">
                        <h4 class="card-title">Atur Jumlah dan Tambah Keranjang</h4>
                        <p class="card-text"><?= $produkdetail['deskripsi'] ?></p>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Nama Produk: <?php echo $produkdetail['nama']; ?></li>
                        <li class="list-group-item">SKU: <?php echo $produkdetail['sku']; ?></li>
                        <li class="list-group-item">Katergori: <?php echo $produkdetail['kategori']; ?></li>
                        <li class="list-group-item">Harga: <?php echo 'Rp ' . number_format($produkdetail['biaya']); ?>
                        </li>
                        <br>
                        <div class="row">
                            <label for="">Jumlah:</label>

                            <div class="">
                                <input type="number" name="quantity" id="<?php echo $produkdetail['id_stok']; ?>"
                                    value="1" class="quantity form-control">
                            </div>

                            <?php if ($this->session->userdata('login')) : ?>
                            <button class="add_cart btn btn-success btn-block"
                                data-produkid="<?php echo $produkdetail['id_stok']; ?>"
                                data-produknama="<?php echo $produkdetail['nama']; ?>"
                                data-produkharga="<?php echo $produkdetail['biaya']; ?>">Add To Cart</button>
                            <?php endif; ?>
                            <?php if (!$this->session->userdata('login')) : ?>
                            <li>
                                <a class="btn btn-default btn-red" href="<?php echo base_url(); ?>users/login">Login
                                    untuk
                                    buat Tambah Keranjang</a>
                            </li>
                            <?php endif; ?>

                        </div>
                    </ul>

                </div>
            </div>
        </div>

</section>

<!-- The Modal -->
<div id="myModal" class="modal">

    <!-- Modal content -->
    <div class="modal-content container">
        <span class="close">&times;</span>
        <br>
        <h4>Shopping Cart</h4>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Produk</th>
                    <th>Harga</th>
                    <th>Qty</th>
                    <th>Subtotal</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody id="detail_cart">

            </tbody>

        </table>
        <a href="<?php echo base_url(); ?>users/dashboard" class="btn btn-success">
            Selesaikan Belanjaan
        </a>
        <br>
        <br>
    </div>

</div>

<footer class="wow fadeInUp" data-wow-delay=".8s">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <a class="footer-logo" href="#">
                    <img class="img-responsive" src="images/footer-logo.png" alt="">
                </a>
                <p>Copyright © 2021 <a href="#">Prime Dental Studio</a></p>

            </div>
        </div>
    </div>
</footer>