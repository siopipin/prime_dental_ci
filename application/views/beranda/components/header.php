<!DOCTYPE html>
<html class="no-js">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Prime Dental Studio</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= base_url('assets/store/'); ?>css/bootstrap.min.css">

    <link rel="stylesheet" href="<?= base_url('assets/store/'); ?>css/custom.css">
    <link rel="stylesheet" href="<?= base_url('assets/store/'); ?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/store/'); ?>css/main.css">
    <link rel="stylesheet" href="<?= base_url('assets/store/'); ?>css/animate.css">
    <link rel="stylesheet" href="<?= base_url('assets/store/'); ?>css/responsive.css">
</head>

<body>