<script src="<?= base_url('assets/store/'); ?>js/vendor/modernizr-2.6.2.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>


<script src="<?= base_url('assets/store/'); ?>js/vendor/jquery-1.10.2.min.js"></script>
<script src="<?= base_url('assets/store/'); ?>js/bootstrap.min.js"></script>
<script src="<?= base_url('assets/store/'); ?>js/plugins.js"></script>
<script src="<?= base_url('assets/store/'); ?>js/main.js"></script>
<script src="<?= base_url('assets/store/'); ?>js/wow.min.js"></script>
<script>
new WOW().init();
</script>

<script>
(function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-57708809-1', 'auto');
ga('send', 'pageview');



$(document).ready(function() {
    var url = window.location.pathname;
    var pathname = url.split('/')[3]
    var idpengguna =
        <?php echo $this->session->userdata('user_id'); ?>;
    console.log(pathname);
    var info = document.getElementById("info");

    if (pathname === 'dashboard' && idpengguna != null) {

        $('#detail_cart2').load("<?php echo base_url(); ?>cart/load_cart");
        info.style.display = "block";



    }

    $('#detail_cart').load("<?php echo base_url(); ?>cart/load_cart");


    var modal = document.getElementById("myModal");
    var span = document.getElementsByClassName("close")[0];

    console.log('hello');
    $('.add_cart').click(function() {
        console.log('add_cart');

        var produk_id = $(this).data("produkid");
        var produk_nama = $(this).data("produknama");
        var produk_harga = $(this).data("produkharga");
        var quantity = $('#' + produk_id).val();

        console.log(produk_id);
        console.log(produk_harga);
        console.log(produk_nama);
        console.log(quantity);



        $.ajax({
            url: '<?php echo base_url(); ?>cart/add_to_cart',
            method: 'POST',
            datatype: 'json',
            data: JSON.stringify({
                'id_stok': produk_id,
                'nama': produk_nama,
                'biaya': produk_harga,
                'quantity': quantity
            }),
            success: function(data) {
                console.log(data);
                $('#detail_cart').load("<?php echo base_url(); ?>index.php/cart/load_cart");
                modal.style.display = "block";
                // $('#detail_cart').html(data);
            },
            error: function(res) {
                console.log('error');
                console.log(res);
            }
        });
    });

    if ($('.close').length > 0) {
        span.onclick = function() {
            modal.style.display = "none";
        }
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }

        if (event.target == info) {
            info.style.display = "none";
        }
    }

    //Hapus Item Cart
    $(document).on('click', '.hapus_cart', function() {
        var row_id = $(this).attr("id"); //mengambil row_id dari artibut id
        $.ajax({
            url: "<?php echo base_url(); ?>cart/hapus_cart",
            method: "POST",
            data: {
                row_id: row_id
            },
            success: function(data) {
                $('#detail_cart').html(data);
            }
        });
    });






    $('.simpan_transaksi').click(function() {
        var nama = document.getElementById('nama').value;
        var hp = document.getElementById('hp').value;
        var alamat = document.getElementById('alamat').value;
        userid = <?php echo $this->session->userdata('user_id'); ?>;

        //cek data keranjang
        datas = <?php echo json_encode($cart) ?>;
        item = [];
        for (key in datas) {
            var val = datas[key];
            item.push(val);
        }

        $.ajax({
            url: '<?php echo base_url(); ?>cart/save_transaksi',
            method: 'POST',
            datatype: 'json',
            data: JSON.stringify({
                'nama': nama,
                'alamat': alamat,
                'hp': hp,
                'iduser': userid,
                'data': item
            }),
            success: function(res) {
                if (res != 0) {
                    // On success redirect.
                    location.reload();
                } else
                    console.log('wrong');
            },
            error: function(res) {
                console.log('error');
                console.log(res);
            }
        });

    });

    //Penjadwalan
    userid =
        <?php echo $this->session->userdata('user_id'); ?>;

    $("#tambahpenjadwalan").click(function() {
        console.log('hallo');
        var id = userid;
        var
            namapasien = document.getElementById('nama').value;
        var
            keluhan = document.getElementById('keluhan').value;
        var
            tgl = document.getElementById('tgl').value;

        $.ajax({
            url: '<?php echo base_url(); ?>administrator/rekam-medis/kunjungan-jadwal',
            method: 'POST',
            datatype: 'json',
            data: JSON.stringify({
                'idpasien': id,
                'nama': namapasien,
                'tgl': tgl,
                'keluhan': keluhan
            }),
            success: function(res) {
                if (res != 0) {
                    // On success redirect.
                    window.location.replace(res);
                } else
                    console.log('wrong');
            },
            error: function(res) {
                console.log('error');
                console.log(res);
            }
        });

    });

});
</script>

</body>



</html>