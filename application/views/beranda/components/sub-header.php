<header>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-6 col-sm-3">
                <a href="#" class="logo">
                    <img src="images/logo.png" alt="">
                </a>
            </div>
            <div class="col-md-6 col-xs-6 col-sm-6">
                <div class="menu mt-3">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">

                                    <li><a href="<?php echo base_url(); ?>">Prime Dental</a></li>
                                    <li><a href="<?php echo base_url(); ?>#services">Services</a></li>
                                    <li><a href="<?php echo base_url(); ?>#products">Products</a></li>
                                    <li><a href="<?php echo base_url(); ?>#testimonials">Testimonials</a></li>
                                    <li><a href="<?php echo base_url(); ?>#contact">Contact</a></li>
                                </ul>



                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </div>
            </div>
            <div class="col-md-3 col-xs-12 col-sm-3">
                <ul class="">
                    <ul class="nav navbar-nav navbar-right">

                        <?php if (!$this->session->userdata('login')) : ?>
                        <li><a class="btn btn-primary text-white"
                                href="<?php echo base_url(); ?>users/register">Register</a></li>

                        <li><a class="ml-1 btn btn-primary text-white"
                                href="<?php echo base_url(); ?>users/login">Login</a>
                        </li>
                        <?php endif; ?>
                        <?php if ($this->session->userdata('login')) : ?>
                        <li><a class="warnatext"
                                href="<?php echo base_url(); ?>users/dashboard"><?php echo $this->session->userdata('username'); ?></a>
                        </li>
                        <li><a class="btn btn-primary text-white"
                                href="<?php echo base_url(); ?>users/logout">Logout</a></li>
                        <?php endif; ?>
                    </ul>
                </ul>
            </div>
        </div>
    </div>
</header>