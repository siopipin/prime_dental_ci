<section id="home">
    <div class="container">
        <div class="row">

            <div class="col-md-4 col-md-offset-4">
                <br><br><br><br><br>
                <a href="<?php echo base_url(); ?>" class="text-center">
                    <img src="<?php echo base_url(); ?>assets/images/logo-min.png" alt="logo.png">
                </a>
                <?php echo validation_errors(); ?>
                <?php echo form_open_multipart('users/register'); ?>
                <h3 class="text-center"><?= $title ?></h3>
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="name" placeholder="Nama">
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" placeholder="Username">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control" placeholder="Email">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" class="form-control" name="password2" placeholder="Confirm Password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <?php echo form_close() ?>
            </div>
        </div>
    </div>
</section>
<br>
<br>

<footer class="wow fadeInUp" data-wow-delay=".8s">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <a class="footer-logo" href="#">
                    <img class="img-responsive" src="images/footer-logo.png" alt="">
                </a>
                <p>Copyright © 2021 <a href="#">Prime Dental Studio</a></p>

            </div>
        </div>
    </div>
</footer>