<section id="home">
    <div class="container">
        <br>
        <br>
        <br><br><br><br>
        <div class="row">
            <div class="col-md-4">
                <div class="block">
                    <h1>
                        Detail Transaksi
                    </h1>
                    <p>Selamat Datang <?php echo $this->session->userdata('username'); ?></p>
                </div>
            </div>
            <hr>
            <div class="col-md-8">
                <div class="block">
                    <h1>
                        Informasi Pembayaran
                    </h1>
                    <ul>
                        <li>Nama Bank: BRI</li>
                        <li>Nomor Rekening: 009670-008882-001</li>
                        <li>A.N: Kelvin Tantono</li>
                    </ul>
                </div>
            </div>


        </div>
        <hr>
        <div class="row">
            <h2>Detail Transaksi</h2>
            <div class="col-md-6">
                <h4>Belanjaan</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>SKU</th>
                            <th>Nama Produk</th>
                            <th>Kategori</th>
                            <th>Qty</th>
                            <th>Total</th>

                        </tr>
                    </thead>
                    <tbody id="detail_cart">
                        <td><?php echo $detailtransaksi['sku']; ?></td>
                        <td><?php echo $detailtransaksi['nama']; ?></td>
                        <td><?php echo $detailtransaksi['kategori']; ?></td>
                        <td><?php echo $detailtransaksi['jumlah']; ?></td>
                        <td><?php echo $detailtransaksi['total']; ?></td>
                    </tbody>

                </table>
                <h3>Total Harus Bayar: <?php echo 'Rp ' . number_format($totalbayar['totalbayar']); ?> </h3>

            </div>

            <div class="col-md-6">
                <?php echo form_open_multipart('pages/updatedetailtransaksi'); ?>
                <input type="hidden" name="id" class="form-control"
                    value="<?php echo $detailtransaksi['kode_transaksi']; ?>">
                <h2>Data Bukti Pembayaran</h2>
                <div class="form-group">
                    <label for="">Dibayar Oleh</label>
                    <input type="text" name="namapengirim" id="nama" class="form-control"
                        placeholder="Isi Nama Pengirim" required autofocus>
                </div>
                <div class="form-group">
                    <label for="">Upload Bukti Bayar</label>
                    <input type="file" name="userfile" id="avatar" accept="image/png, image/jpeg, image/jpg, image/gif"
                        class="form-control" placeholder="Isi Alamat Penerima" required autofocus>
                </div>
                <button class="simpan_transaksi btn btn-primary btn-block">Kirim Bukti</button>

                <?php echo form_close() ?>
            </div>
        </div>
        <hr>


</section>

<footer class="wow fadeInUp" data-wow-delay=".8s">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <a class="footer-logo" href="#">
                    <img class="img-responsive" src="images/footer-logo.png" alt="">
                </a>
                <p>Copyright © 2021 <a href="#">Prime Dental Studio</a></p>

            </div>
        </div>
    </div>
</footer>