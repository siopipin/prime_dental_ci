<section id="home">
    <div class="container">

        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">



            <div class="carousel-inner" role="listbox">

                <div class="item active">
                    <div class="carousel-caption">
                        <div class="row">
                            <?php echo validation_errors(); ?>
                            <?php echo form_open('users/login'); ?>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <h1 class="text-center"><?= $title ?></h1>
                                    <div class="form-group">
                                        <!--  <label>Username</label> -->
                                        <input type="text" name="email" class="form-control" placeholder="Enter Email"
                                            required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <!-- <label>Password</label> -->
                                        <input type="password" class="form-control" name="password"
                                            placeholder="Enter Password" required autofocus>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block">Login</button>
                                </div>
                            </div>
                            <?php echo form_close() ?>

                            <br>
                            <hr>
                            <p style="color:black;text-align:center;">Akses Admin</p>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-4">
                                    <a href="<?php echo base_url(); ?>administrator"
                                        class="btn btn-primary btn-block">Login</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>




            </div>
        </div>
</section>

<footer class="wow fadeInUp" data-wow-delay=".8s">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <a class="footer-logo" href="#">
                    <img class="img-responsive" src="images/footer-logo.png" alt="">
                </a>
                <p>Copyright © 2021 <a href="#">Prime Dental Studio</a></p>

            </div>
        </div>
    </div>
</footer>