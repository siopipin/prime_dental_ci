<section id="home">
    <div class="container">
        <br>
        <br>
        <br><br><br><br>
        <div class="row">
            <div class="col-md-4">
                <div class="block">
                    <h1>
                        Beranda
                    </h1>
                    <p>Selamat Datang <?php echo $this->session->userdata('username'); ?></p>
                </div>
            </div>


        </div>
        <hr>
        <div class="row">
            <h2>Keranjang Belanja</h2>
            <div class="col-md-6">
                <h4>Shopping Cart</h4>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Produk</th>
                            <th>Harga</th>
                            <th>Qty</th>
                            <th>Subtotal</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="detail_cart">

                    </tbody>

                </table>
            </div>

            <div class="col-md-6">

                <h2>Informasi Penerima</h2>
                <div class="form-group">
                    <label for="">Nama Penerima</label>
                    <input type="text" name="nama" id="nama" class="form-control" placeholder="Isi Nama Penerima"
                        required autofocus>
                </div>
                <div class="form-group">
                    <label for="">HP Penerima</label>
                    <input type="number" name="hp" id="hp" class="form-control" placeholder="Isi Nomor HP Penerima"
                        required autofocus>
                </div>
                <div class="form-group">
                    <label for="">Alamat Penerima</label>
                    <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Isi Alamat Penerima"
                        required autofocus>
                </div>
                <button class="simpan_transaksi btn btn-primary btn-block">Simpan Transaksi</button>

                <?php echo form_close() ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="block">
                <div>
                    <p class="text-primary mb-2">
                        <i class="fa fa-user"></i> Booking dan Riwayat Medis -
                        <?= $this->session->userdata('username'); ?>
                    </p>
                </div>
                <div class="row container">
                    <div class="col">
                        <div class="card">
                            <div class="card-header bg-dark text-dark font-weight-bold text-center">
                                Riwayat Kunjungan</div>
                            <div class="card-body container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Jadwal Kunjungan</th>
                                                <th>Keluhan</th>
                                                <th>Diagnosa</th>
                                                <th>Penatalaksanaan</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;
                                            foreach ($riwayatkunjungan as $r) : ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td><?= $r['tgl_kunjungan']; ?></td>
                                                <td><?= $r['keluhan']; ?></td>
                                                <td><?= $r['id_perawatan']; ?></td>
                                                <td><?= $r['penatalaksaan']; ?></td>
                                                <td><?= ($r['status'] == 0 ? 'Pending' : 'Aktif'); ?></td>

                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card">
                            <div class="card-header bg-dark text-dark font-weight-bold text-center">
                                Riwayat Transaksi</div>
                            <div class="card-body container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Tgl Transaksi</th>
                                                <th>Kode Trans.</th>
                                                <th>Nama Penerima</th>
                                                <th>HP Penerima</th>
                                                <th>Total</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;
                                            foreach ($riwayattransaksi as $r) : ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td><?= $r['tgl_transaksi']; ?></td>
                                                <td> <a
                                                        href="<?php echo base_url(); ?>pages/detailtransaksi/<?php echo $r['kode_transaksi']; ?>"><?= $r['kode_transaksi']; ?></a>
                                                </td>
                                                <td><?= $r['nama_penerima']; ?></td>

                                                <td><?= $r['hp_penerima']; ?></td>
                                                <td><?= $r['total']; ?></td>
                                                <td><?= ($r['status'] == 0) ? 'Pending' : (($r['status'] == 1) ? 'Telah kirim bukti bayar' : (($r['status'] == 2) ? 'Accept/Pengiriman' : '-')); ?>
                                                </td>

                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>


</section>


<!-- The Modal -->
<div id="info" class="modal">

    <!-- Modal content -->
    <div class="modal-content container">
        <span class="close">&times;</span>
        <br>
        <h4>Selamat datang <b><?php echo $this->session->userdata('username'); ?></b>, jangan lupa untuk menyelesaikan
            keranjang
            belanja ya.
        </h4>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Produk</th>
                    <th>Harga</th>
                    <th>Qty</th>
                    <th>Subtotal</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody id="detail_cart2">

            </tbody>

        </table>
        <a href="<?php echo base_url(); ?>users/dashboard" class="btn btn-success">
            Selesaikan Belanjaan
        </a>
        <br>
        <br>
    </div>

</div>

<footer class="wow fadeInUp" data-wow-delay=".8s">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <a class="footer-logo" href="#">
                    <img class="img-responsive" src="images/footer-logo.png" alt="">
                </a>
                <p>Copyright © 2021 <a href="#">Prime Dental Studio</a></p>

            </div>
        </div>
    </div>
</footer>