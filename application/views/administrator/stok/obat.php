<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/assets/pages/data-table/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/ekko-lightbox/dist/ekko-lightbox.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/lightbox2/dist/css/lightbox.css">


<div class="page-header">
    <div class="page-header-title">
        <h4>Stok</h4>
    </div>
    <div class="page-header-breadcrumb">
        <ul class="breadcrumb-title">
            <li class="breadcrumb-item">
                <a href="index-2.html">
                    <i class="icofont icofont-home"></i>
                </a>
            </li>
            <li class="breadcrumb-item"><a href="#!">Stok</a>
            </li>
            <li class="breadcrumb-item"><a href="#!">Stok</a>
            </li>
        </ul>
    </div>
</div>





<!-- Page-header end -->
<!-- Page-body start -->
<div class="page-body">
    <div>

        <a href="<?php echo base_url(); ?>administrator/stok/obat-tambah" type="button"
            class="btn_tambah_unit_stok btn btn-primary mb-2">

            <!-- Buat link untuk mengarah ke tambah stok -->
            <i class="fas fa-plus"></i> Tambah Stok
        </a>
    </div>
    <!-- DOM/Jquery table start -->



    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-responsive dt-responsive">
                        <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tanggal Masuk</th>
                                    <th>SKU</th>
                                    <th>Nama</th>
                                    <th>Foto</th>
                                    <th>Jenis</th>
                                    <th>Stok</th>
                                    <th>Satuan</th>
                                    <th>Biaya</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($rows as $key => $row) : ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo date(
                                                'M d,Y H:i',
                                                strtotime($row['tanggal_masuk'])
                                            ); ?></td>

                                    <td><?php echo $row['sku']; ?></td>

                                    <td><?php echo $row['nama']; ?></td>
                                    <td>
                                        <img src="<?php echo base_url() .
                                                            'assets/images/products/' .
                                                            $row['img']; ?>" width="70px">
                                    </td>
                                    <td><?php echo $row['jenis']; ?></td>

                                    <!-- Tampilkan gambar dalam tabel -->



                                    <!-- Jenis dalam bentuk select options -->
                                    <td><?php echo $row['stok']; ?></td>
                                    <td><?php echo $row['satuan']; ?></td>
                                    <td><?php echo $row['biaya']; ?></td>
                                    <td>
                                        <?php if ($row['status'] == 1) { ?>
                                        <a class="label label-inverse-primary enable"
                                            href='<?php echo base_url(); ?>administrator/stok/obat/enable/<?php echo $row['id_stok']; ?>?table=<?php echo base64_encode('tbl_stok'); ?>'>Enabled</a>
                                        <?php } else { ?>
                                        <a class="label label-inverse-warning desable"
                                            href='<?php echo base_url(); ?>administrator/stok/obat/disable/<?php echo $row['id_stok']; ?>?table=<?php echo base64_encode('tbl_stok'); ?>'>Disabled</a>
                                        <?php } ?>
                                        <a class="label label-inverse-info"
                                            href='<?php echo base_url(); ?>administrator/stok/obat/edit-obat/<?php echo $row['id_stok']; ?>'>Edit</a>
                                        <a class="label label-inverse-danger delete"
                                            href='<?php echo base_url(); ?>administrator/perawatan/delete/<?php echo $row['id_stok']; ?>?table=<?php echo base64_encode('tbl_stok'); ?>'>Delete</a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- DOM/Jquery table end -->
</div>