   <link rel="stylesheet" type="text/css"
       href="<?php echo base_url(); ?>admintemplate/bower_components/switchery/dist/switchery.min.css">
   <link rel="stylesheet" type="text/css"
       href="<?php echo base_url(); ?>admintemplate/assets/pages/advance-elements/css/bootstrap-datetimepicker.css">
   <!-- Date-range picker css  -->
   <link rel="stylesheet" type="text/css"
       href="<?php echo base_url(); ?>admintemplate/bower_components/bootstrap-daterangepicker/daterangepicker.css" />
   <!-- Date-Dropper css -->
   <link rel="stylesheet" type="text/css"
       href="<?php echo base_url(); ?>admintemplate/bower_components/datedropper/datedropper.min.css" />
   <link rel="stylesheet" type="text/css"
       href="<?php echo base_url(); ?>admintemplate/bower_components/switchery/dist/switchery.min.css" />


   <div class="page-header">
       <div class="page-header-title">
           <h4>Layanan Perawatan</h4>
       </div>
       <div class="page-header-breadcrumb">
           <ul class="breadcrumb-title">
               <li class="breadcrumb-item">
                   <a href="index-2.html">
                       <i class="icofont icofont-home"></i>
                   </a>
               </li>
               <li class="breadcrumb-item"><a href="#!">Perawatan</a>
               </li>
               <li class="breadcrumb-item"><a href="#!">Update Perawatan</a>
               </li>
           </ul>
       </div>
   </div>
   <!-- Page header end -->
   <!-- Page body start -->
   <div class="page-body">
       <div class="row">
           <div class="col-sm-12">
               <div class="alert alert-warning alert-dismissible fade show" id="warning" role="alert">
                   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
                   </button>
               </div>
               <!-- Basic Form Inputs card start -->
               <div class="card">
                   <div class="card-header">
                       <h5>Update Stok -- <small style="text-decoration: underline;"><?php echo $row['nama']; ?></small>
                       </h5>
                       <div class="card-header-right">
                           <i class="icofont icofont-rounded-down"></i>
                           <i class="icofont icofont-refresh"></i>
                           <i class="icofont icofont-close-circled"></i>
                       </div>
                   </div>
                   <div class="card-block">
                       <div class="col-sm-8">
                           <input type="hidden" name="id" id="id" class="form-control"
                               value="<?php echo $row['id_riwayat_stok']; ?>">
                           <div class="form-group row">
                               <label class="col-sm-2 col-form-label">Nama Stok</label>
                               <div class="col-sm-10">
                                   <input type="text" name="nama" class="form-control"
                                       value="<?php echo $row['nama']; ?>" placeholder="Nama Perawatan">
                               </div>
                           </div>

                           <div class="form-group row">
                               <label class="col-sm-2 col-form-label">Stok</label>
                               <div class="col-sm-10">
                                   <input type="text" name="stok" id="stok" value="<?php echo $row['stok']; ?>"
                                       class="form-control" placeholder="Stok">
                               </div>
                           </div>
                           <div class="form-group row">
                               <label class="col-sm-2 col-form-label"></label>
                               <div class="col-sm-10">
                                   <button type="submit" id="update_stok_riwayat" name="submit"
                                       class="btn btn-primary">Update</button>
                               </div>
                           </div>
                           <textarea id="description" style="visibility: hidden;"></textarea>
                           </form>
                       </div>

                   </div>
               </div>
           </div>
           <!-- Basic Form Inputs card end -->


           <script src="<?php echo base_url(); ?>assets/js/jquery-3.6.0.min.js"></script>
           <script src="<?php echo base_url(); ?>assets/js/sweetalert2.all.min.js"></script>

           <script>
           $(document).ready(function() {
               $("#warning").hide();

               $("#update_stok_riwayat").click(function() {
                   var
                       stok = document.getElementById('stok').value;
                   var
                       id = document.getElementById('id').value;
                   if (stok.length == "") {
                       $("#warning").show();
                       $('#warning').append("<span> Jumlah Stok Unit tidak boleh kosong </span>");

                   } else {
                       $("#warning").hide();

                       $.ajax({
                           url: '<?php echo base_url(); ?>administrator/stok/update-riwayat-stok',
                           method: 'POST',
                           datatype: 'json',
                           data: JSON.stringify({
                               'stok': stok,
                               'id_riwayat_stok': id,
                           }),
                           success: function(res) {
                               if (res != 0) {
                                   // On success redirect.
                                   window.location.replace(res);
                               } else
                                   console.log('wrong');
                           },
                           error: function(res) {
                               console.log('error');
                               console.log(res);
                           }
                       });
                   }
               });

           });
           </script>

           <script type="text/javascript"
               src="<?php echo base_url(); ?>admintemplate/bower_components/switchery/dist/switchery.min.js"></script>
           <!-- Custom js -->
           <script type="text/javascript"
               src="<?php echo base_url(); ?>admintemplate/assets/pages/advance-elements/swithces.js"></script>
           <script type="text/javascript"
               src="<?php echo base_url(); ?>admintemplate/assets/pages/advance-elements/moment-with-locales.min.js">
           </script>
           <script type="text/javascript"
               src="<?php echo base_url(); ?>admintemplate/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js">
           </script>
           <script type="text/javascript"
               src="<?php echo base_url(); ?>admintemplate/assets/pages/advance-elements/bootstrap-datetimepicker.min.js">
           </script>
           <!-- Date-range picker js -->
           <script type="text/javascript"
               src="<?php echo base_url(); ?>admintemplate/bower_components/bootstrap-daterangepicker/daterangepicker.js">
           </script>
           <!-- Date-dropper js -->
           <script type="text/javascript"
               src="<?php echo base_url(); ?>admintemplate/bower_components/datedropper/datedropper.min.js"></script>


           <!-- ck editor -->
           <script src="<?php echo base_url(); ?>admintemplate/bower_components/ckeditor/ckeditor.js"></script>
           <!-- echart js -->

           <script src="<?php echo base_url(); ?>admintemplate/assets/pages/user-profile.js"></script>