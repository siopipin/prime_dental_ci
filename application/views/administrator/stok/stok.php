<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/assets/pages/data-table/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/ekko-lightbox/dist/ekko-lightbox.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/lightbox2/dist/css/lightbox.css">


<div class="page-header">
    <div class="page-header-title">
        <h4>Penambahan Stok</h4>
    </div>
    <div class="page-header-breadcrumb">
        <ul class="breadcrumb-title">
            <li class="breadcrumb-item">
                <a href="index-2.html">
                    <i class="icofont icofont-home"></i>
                </a>
            </li>
            <li class="breadcrumb-item"><a href="#!">Stok</a>
            </li>
            <li class="breadcrumb-item"><a href="#!">Riwayat Stok</a>
            </li>
        </ul>
    </div>
</div>





<!-- Page-header end -->
<!-- Page-body start -->
<div class="page-body">
    <div>

        <button type="button" class="btn_tambah_unit_stok btn btn-primary mb-2" data-toggle="modal"
            data-target="#formmodalunitstok">
            <i class="fas fa-plus"></i>Tambah Unit Stok
        </button>
    </div>
    <!-- DOM/Jquery table start -->



    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-responsive dt-responsive">
                        <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Jenis Transaksi</th>
                                    <th>ID Transaksi</th>
                                    <th>SKU</th>
                                    <th>Nama</th>
                                    <th>Foto</th>
                                    <th>Jenis</th>
                                    <th>Stok</th>
                                    <th>Satuan</th>
                                    <th>Catatan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($rows as $key => $row) : ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo date(
                                                'M d,Y H:i',
                                                strtotime($row['tgl_transaksi'])
                                            ); ?></td>
                                    <td><?php
                                            $jenistransaksi = ($row['status'] == 0 ? "Penambahan" : (($row['status'] == 1) ? "Pembelian" : "Pending"));
                                            echo $jenistransaksi; ?></td>
                                    <td><?php echo $row['id_transaksi']; ?></td>

                                    <td><?php echo $row['sku']; ?></td>

                                    <td><?php echo $row['nama']; ?></td>

                                    <!-- Tampilkan gambar dalam tabel -->
                                    <td>
                                        <img src="<?php echo base_url() .
                                                            'assets/images/products/' .
                                                            $row['img']; ?>" width="70px">
                                    </td>


                                    <!-- Jenis dalam bentuk select options -->
                                    <td><?php echo $row['jenis']; ?></td>
                                    <td><?php echo $row['stok']; ?></td>
                                    <td><?php echo $row['satuan']; ?></td>
                                    <td><?php echo $row['notes']; ?></td>
                                    <td>
                                        <a class="label label-inverse-info"
                                            href='<?php echo base_url(); ?>administrator/stok/edit-riwayat-stok/<?php echo $row['id_riwayat_stok']; ?>'>Edit
                                            Stok</a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- DOM/Jquery table end -->
</div>

<!-- Modal Unit Stok -->
<div class="modal fade" id="formmodalunitstok" tabindex="-1" aria-labelledby="formModalLabelDokter" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabelDokter">Tambah Unit Stok</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning alert-dismissible fade show" id="warning" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="form-group">
                    <label for="nama">Pilih Nama Produk</label>
                    <select class="form-control" name="namaproduk" id="namaproduk" required>
                        <option>Select Produk</option>
                        <?php foreach ($stok_grup as $row) :
                        ?>
                        <option value="<?php echo $row['sku']; ?>">
                            <?php echo $row['nama']; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <small class="muted text-danger"><?= form_error(
                                                            'namastok'
                                                        ) ?></small>
                </div>

                <div class="form-group">
                    <label for="nama">Pilih Jenis Transaksi</label>
                    <select class="form-control" name="status" id="status" required>
                        <option>Select Jenis Transaksi</option>
                        <option value="0">Penambahan</option>
                        <option value="1">Pembelian</option>
                    </select>
                    <small class="muted text-danger"><?= form_error(
                                                            'status'
                                                        ) ?></small>
                </div>
                <div class="form-group">
                    <label for="stok">Jumlah Stok</label>

                    <input class="form-control" type="number" name="stokbaru" id="stokbaru" value="">

                    <small class="muted text-danger"><?= form_error(
                                                            'stok'
                                                        ) ?></small>
                </div>
                <div class="form-group">
                    <label for="stok">ID Transaksi</label>

                    <input class="form-control" type="text" name="id_transaksi" id="id_transaksi" value="-">

                    <small class="muted text-danger"><?= form_error(
                                                            'idtransaksi'
                                                        ) ?></small>
                </div>

                <div class="form-group">
                    <label for="stok">Catatan</label>

                    <input class="form-control" type="text" name="notes" id="notes" value="">

                    <small class="muted text-danger"><?= form_error(
                                                            'idtransaksi'
                                                        ) ?></small>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button name="submit" class="btn btn-primary" id="add-stok">Tambah</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.6.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sweetalert2.all.min.js"></script>

<script>
$(document).ready(function() {
    console.log('hallo1');

    //HIDDEN WARNING
    $("#warning").hide();

    var datas = [];
    var newData;
    var userid;
    datas = <?php echo json_encode($rows) ?>;
    console.log(datas[0]);


    function selectedStok() {
        var selected = this.value;
        // VAL = SKU, EX: BO01

        // FETCH DATA TO DATABASE
        fetchStokGrupSelected(selected);

    }


    function fetchStokGrupSelected(sku) {
        console.log(sku);
        $.ajax({
            url: ' <?php echo base_url(); ?>administrator/stok/select',
            method: 'POST',
            datatype: 'json',
            data: JSON.stringify({
                'sku': sku,
            }),
            success: function(res) {
                console.log(res);
                if (res != false) {
                    var tmp = JSON.parse(res);
                    console.log(tmp);
                    newData = tmp[0];
                    console.log(newData);
                } else {
                    console.log('wrong');
                }
            },
            error: function(res) {
                console.log('error');
                console.log(res);
            }
        });
    }
    $("#namaproduk").on("change", selectedStok); //do Ajax to add
    $("#add-stok").click(function() {
        console.log('hallo');
        //HIDDEN WARNING
        $("#warning").hide();

        var
            newstok = document.getElementById('stokbaru').value;
        var
            status = document.getElementById('status').value;
        var
            id_transaksi = document.getElementById('id_transaksi').value;
        var
            notes = document.getElementById('notes').value;
        userid = <?php echo $this->session->userdata('user_id'); ?>;
        console.log(userid);
        if (newstok.length == "") {
            $("#warning").show();
            $('#warning').append("<span> Jumlah Stok Unit tidak boleh kosong </span>");

        } else if (newData == null) {
            $("#warning").show();
            $('#warning').append("<span> Silahkan pilih produk </span>");
        } else {
            $("#warning").hide();

            $.ajax({
                url: '<?php echo base_url(); ?>administrator/stok/add-stok-unit',
                method: 'POST',
                datatype: 'json',
                data: JSON.stringify({
                    'stok': newstok,
                    'status': status,
                    'id_transaksi': id_transaksi,
                    'notes': notes,
                    'id_user': userid,
                    'data': newData
                }),
                success: function(res) {
                    if (res != 0) {
                        // UPDATE STOK KETIKA BERHASIL SIMPAN RIWAYAT
                        updateStok(newData.sku, newData.stok, newstok, status);
                        // On success redirect.
                        window.location.replace(res);
                    } else
                        console.log('wrong');
                },
                error: function(res) {
                    console.log('error');
                    console.log(res);
                }
            });
        }



    });

    function updateStok(sku, stoklama, stokbaru, status) {
        $.ajax({
            url: '<?php echo base_url(); ?>administrator/stok/update-total-stok',
            method: 'POST',
            datatype: 'json',
            data: JSON.stringify({
                'sku': sku,
                'stoklama': stoklama,
                'stokbaru': stokbaru,
                'jenistransaksi': status,
            }),
            success: function(res) {
                console.log(res)
            },
            error: function(res) {
                console.log('error');
                console.log(res);
            }
        });
    }

});
</script>