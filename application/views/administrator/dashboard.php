            <div class="page-header">
                <div class="page-header-title">
                    <h4>Dashboard</h4>
                </div>
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index-2.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Pages</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="page-body">
                <div class="row">
                    <div class="col-md-12 col-xl-4">
                        <!-- table card start -->
                        <div class="card table-card">
                            <div class="">
                                <div class="row-table">
                                    <div class="col-sm-6 card-block-big br">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <i class="icofont icofont-eye-alt text-success"></i>
                                            </div>
                                            <div class="col-sm-8 text-center">
                                                <h5><?php echo $totalstok['total'] ?></h5>
                                                <span>STOK</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 card-block-big">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <i class="icofont icofont-ui-music text-danger"></i>
                                            </div>
                                            <div class="col-sm-8 text-center">
                                                <h5><?php echo $totalobat['total'] ?></h5>
                                                <span>OBAT</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="row-table">
                                    <div class="col-sm-6 card-block-big br">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <i class="icofont icofont-files text-info"></i>
                                            </div>
                                            <div class="col-sm-8 text-center">
                                                <h5><?php echo $totalproduk['total'] ?></h5>
                                                <span>PRODUK</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 card-block-big">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <i class="icofont icofont-envelope-open text-warning"></i>
                                            </div>
                                            <div class="col-sm-8 text-center">
                                                <h5><?php echo $totalbahan['total'] ?></h5>
                                                <span>BAHAN</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- table card end -->
                    </div>
                    <div class="col-md-12 col-xl-4">
                        <!-- table card start -->
                        <div class="card table-card">
                            <div class="">
                                <div class="row-table">
                                    <div class="col-sm-6 card-block-big br">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div id="barchart" style="height:40px;width:40px;"></div>
                                            </div>
                                            <div class="col-sm-8 text-center">
                                                <h5><?php echo $totaltransaksi['total'] ?></h5>
                                                <span>Transaksi</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 card-block-big">
                                        <div class="row ">
                                            <div class="col-sm-4">
                                                <i class="icofont icofont-network text-primary"></i>
                                            </div>
                                            <div class="col-sm-8 text-center">
                                                <h5><?php echo $totaluser['total'] ?></h5>
                                                <span>User</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="row-table">
                                    <div class="col-sm-6 card-block-big br">
                                        <div class="row ">
                                            <div class="col-sm-4">
                                                <div id="barchart2" style="height:40px;width:40px;"></div>
                                            </div>
                                            <div class="col-sm-8 text-center">
                                                <h5><?php echo $totalrekammedis['total'] ?></h5>
                                                <span>Rekam Medis</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 card-block-big">
                                        <div class="row ">
                                            <div class="col-sm-4">
                                                <i class="icofont icofont-network-tower text-primary"></i>
                                            </div>
                                            <div class="col-sm-8 text-center">
                                                <h5><?php echo $totallayanan['total'] ?></h5>
                                                <span>Layanan</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- table card end -->
                    </div>
                    <div class="col-md-12 col-xl-4">
                        <!-- widget primary card start -->
                        <div class="card table-card widget-primary-card">
                            <div class="">
                                <div class="row-table">
                                    <div class="col-sm-3 card-block-big">
                                        <i class="icofont icofont-star"></i>
                                    </div>
                                    <div class="col-sm-9">
                                        <h4><?php echo 'Rp ' .  number_format($totalpendapatan['total']) ?></h4>
                                        <h6>Pendapatan dari Transaksi</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- widget primary card end -->
                        <!-- widget-success-card start -->
                        <div class="card table-card widget-success-card">
                            <div class="">
                                <div class="row-table">
                                    <div class="col-sm-3 card-block-big">
                                        <i class="icofont icofont-trophy-alt"></i>
                                    </div>
                                    <div class="col-sm-9">
                                        <h4><?php echo $totaltransaksiselesai['total'] ?></h4>
                                        <h6>Transaksi Selesai</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- widget-success-card end -->
                    </div>

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-6">
                                <form class='well form-vertical' method="POST">
                                    <?php
                                    $folder = FCPATH . 'assets/logs/';
                                    $namafile = "";
                                    if (is_dir($folder)) {
                                        $files = opendir($folder);
                                        if ($files) {
                                            while (($file = readdir($files)) !== false) {
                                                if ($file != "." && $file != ".." && $file != "db.json" && $file != "result.json") {
                                                    $namafile = $namafile . "<option value='" . $file . "'>$file</option>";
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <div class="form-group">
                                        <label for="">Pilih Riwayat Access Log Sebelumnya</label>
                                        <select name="filelama">
                                            <option selected>Pilih Riwayat Access Log</option>
                                            <?php echo $namafile; ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">URL Filter (regexp)</label>
                                        <input type="text" name="urlfilter" id="deskripsi" class="form-control span5"
                                            value='<?php echo isset($_POST['urlfilter']) ? $_POST['urlfilter'] : '' ?>' />
                                        <small class="muted text-primary">Contoh: \.jpg$</small>
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Exclude dependencies</label>
                                        <input type="checkbox" name="nodep" id="deskripsi" class="form-control span1"
                                            value='1'
                                            <?php echo isset($_POST['nodep']) && $_POST['nodep'] == '1' ? 'checked' : '' ?> />
                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Log Filter (regexp)</label>
                                        <input type="text" name="logfilter" id="deskripsi" class="form-control span5"
                                            value='<?php echo isset($_POST['logfilter']) ? $_POST['logfilter'] : '' ?>' />
                                        <small class="muted text-primary">Contoh: Mozilla</small>

                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Start date</label>
                                        <input type="text" name="start_date" id="deskripsi" class="form-control span5"
                                            value='<?php echo isset($_POST['start_date']) ? $_POST['start_date'] : ''  ?>' />
                                        <small class="muted text-primary">Contoh: 22/11/2013 08:00</small>

                                    </div>


                                    <div class="form-group">
                                        <label for="deskripsi">End date</label>
                                        <input type="text" name="end_date" id="deskripsi" class="form-control span5"
                                            value='<?php echo isset($_POST['end_date']) ? $_POST['end_date'] : '' ?>' />
                                        <small class="muted text-primary">Contoh: 01/12/2013 08:00</small>

                                    </div>

                                    <div class="form-group">
                                        <label for="deskripsi">Histogram period</label>
                                        <input type="text" name="histo_period" class="form-control span5"
                                            value='<?php echo isset($_POST['histo_period']) ? $_POST['histo_period'] : '3600' ?>' />
                                    </div>


                                    <div class="form-group">
                                        <button type='submit' class='btn'>Analyze</button>
                                    </div>

                                </form>
                            </div>

                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        Cara mendapatkan Access Log
                                    </div>
                                    <div class="container">
                                        <div class="card-body">
                                            <br>
                                            <div class="list-group">

                                                <a href="https://nur.mekanikserver.com:2222/CMD_SHOW_LOG?domain=primedental.my.id&type=log"
                                                    class="list-group-item list-group-item-action active">
                                                    Download Log Access Dari Server (klik ini)</a>
                                                <a class="list-group-item list-group-item-action ">Username:
                                                    primeden</a>
                                                <a class="list-group-item list-group-item-action ">Password:
                                                    Primedental12345</a>
                                                <a class="list-group-item list-group-item-action ">Kemudian
                                                    simpang (CTRL + S) dengan nama file "access.log" (harus menggunakan
                                                    extensi .log)</a>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <?php echo form_open_multipart('administrator/accesslog/upload'); ?>
                                        <div class="form-group">
                                            <label>Upload Access Log</label>
                                            <input type="file" name="accesslogfile" id="deskripsi"
                                                class="form-control span5" />
                                            <small class="muted text-primary">Contoh: ./log1.log</small>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary" type='submit' class='btn'>Upload</button>
                                        </div>
                                        </form>

                                    </div>
                                    <div class="card-footer text-muted">
                                        @PrimeDental-2021
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php

                                if (isset($_POST['logpath'])) {
                                    $target_dir = FCPATH . './assets/logs/';
                                    $dir = FCPATH . 'assets/logs/';

                                    $target_file = $target_dir . basename($_FILES["logpath"]["name"]);

                                    $uploadOk = 1;
                                    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
                                    //UPLOAD FILE
                                    $errors = array();
                                    $file_name = $_FILES['logpath']['name'] . date('D, d M Y H:i:s');
                                    $file_size = $_FILES['logpath']['size'];
                                    $file_tmp = $_FILES['logpath']['tmp_name'];
                                    $file_type = $_FILES['logpath']['type'];
                                    $file_ext = explode('.', $_FILES['logpath']['name']);

                                    $extensions = end($file_ext);
                                    $urlfile = $dir . basename($file_name);


                                    if ($file_size > 10097152) {
                                        $errors[] = 'File size must be excately 2 MB';
                                    }

                                    if (empty($errors) == true) {
                                        move_uploaded_file($file_tmp, FCPATH . './assets/logs/' . $file_name);

                                        //Upload
                                        //Get information
                                        $url_filter = isset($_POST['urlfilter']) ? $_POST['urlfilter'] : '';
                                        if (trim($url_filter) == '') {
                                            $url_filter = false;
                                        }
                                        $log_filter = isset($_POST['logfilter']) ? $_POST['logfilter'] : '';
                                        if (trim($log_filter) == '') {
                                            $log_filter = false;
                                        }

                                        $nodep = isset($_POST['nodep']) ? $_POST['nodep'] == '1' : false;

                                        $start_date = isset($_POST['start_date']) ? $_POST['start_date'] : '';
                                        if (trim($start_date) == '') {
                                            $start_date = false;
                                        }
                                        $end_date = isset($_POST['end_date']) ? $_POST['end_date'] : '';

                                        $histo_period = isset($_POST['histo_period']) ? $_POST['histo_period'] : '3600';
                                        if (trim($end_date) == '') {
                                            $end_date = false;
                                        }

                                        require_once(APPPATH . 'lib/lib.inc.php');
                                        try {
                                            $log = new LogAnalyzer($urlfile, $url_filter, $log_filter, $nodep, $start_date, $end_date, $histo_period);
                                            include(APPPATH . 'lib/_log_results.php');
                                        } catch (Exception $e) {
                                            echo "<div class='alert alert-error'> Error: " . $e->getMessage() . "</div>";
                                        }

                                        echo "Success";
                                    } else {
                                        print_r($errors);
                                    }

                                    //end//

                                } else if (isset($_POST['filelama']) && $_POST['filelama'] != 'Pilih Riwayat Access Log') {
                                    //Upload
                                    $selected = $_POST['filelama'];
                                    $dir = FCPATH . 'assets/logs/' . $selected;
                                    echo $dir;

                                    //Get information
                                    $url_filter = isset($_POST['urlfilter']) ? $_POST['urlfilter'] : '';
                                    if (trim($url_filter) == '') {
                                        $url_filter = false;
                                    }
                                    $log_filter = isset($_POST['logfilter']) ? $_POST['logfilter'] : '';
                                    if (trim($log_filter) == '') {
                                        $log_filter = false;
                                    }

                                    $nodep =  '1';

                                    $start_date = isset($_POST['start_date']) ? $_POST['start_date'] : '';
                                    if (trim($start_date) == '') {
                                        $start_date = false;
                                    }
                                    $end_date = isset($_POST['end_date']) ? $_POST['end_date'] : '';

                                    $histo_period = isset($_POST['histo_period']) ? $_POST['histo_period'] : '3600';
                                    if (trim($end_date) == '') {
                                        $end_date = false;
                                    }

                                    require_once(APPPATH . 'lib/lib.inc.php');
                                    try {
                                        $log = new LogAnalyzer($dir, $url_filter, $log_filter, $nodep, $start_date, $end_date, $histo_period);
                                        include(APPPATH . 'lib/_log_results.php');
                                    } catch (Exception $e) {
                                        echo "<div class='alert alert-error'> Error: " . $e->getMessage() . "</div>";
                                    }
                                } else {
                                    //Upload
                                    $dir = FCPATH . 'assets/logs/LOG.log';
                                    echo $dir;

                                    //Get information
                                    $url_filter = isset($_POST['urlfilter']) ? $_POST['urlfilter'] : '';
                                    if (trim($url_filter) == '') {
                                        $url_filter = false;
                                    }
                                    $log_filter = isset($_POST['logfilter']) ? $_POST['logfilter'] : '';
                                    if (trim($log_filter) == '') {
                                        $log_filter = false;
                                    }

                                    $nodep =  '1';

                                    $start_date = isset($_POST['start_date']) ? $_POST['start_date'] : '';
                                    if (trim($start_date) == '') {
                                        $start_date = false;
                                    }
                                    $end_date = isset($_POST['end_date']) ? $_POST['end_date'] : '';

                                    $histo_period = isset($_POST['histo_period']) ? $_POST['histo_period'] : '3600';
                                    if (trim($end_date) == '') {
                                        $end_date = false;
                                    }

                                    require_once(APPPATH . 'lib/lib.inc.php');
                                    try {
                                        $log = new LogAnalyzer($dir, $url_filter, $log_filter, $nodep, $start_date, $end_date, $histo_period);
                                        include(APPPATH . 'lib/_log_results.php');
                                    } catch (Exception $e) {
                                        echo "<div class='alert alert-error'> Error: " . $e->getMessage() . "</div>";
                                    }
                                }

                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <h2>DATA PEMBELIAN</h2>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="table-responsive dt-responsive">
                                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Kode Transaksi</th>
                                                        <th>Tgl</th>
                                                        <th>img</th>
                                                        <th>Nama Pembeli</th>
                                                        <th>Produk</th>
                                                        <th>Total</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach ($rows as $key => $row) : ?>
                                                    <tr>
                                                        <td><?php echo $key + 1; ?></td>
                                                        <td><?php echo $row['kode_transaksi']; ?></td>
                                                        <td><?php echo date(
                                                                    'M d,Y H:i',
                                                                    strtotime($row['tgl_transaksi'])
                                                                ); ?></td>

                                                        <td>
                                                            <img src="<?php echo base_url() .
                                                                                'assets/images/bukti/' .
                                                                                $row['img']; ?>" width="70px">
                                                        </td>
                                                        <td><?php echo $row['name']; ?></td>

                                                        <td><?php echo $row['namastok']; ?></td>
                                                        <td><?php echo $row['total']; ?></td>

                                                        <td>
                                                            <?php if ($row['status'] == 1) { ?>
                                                            <a class="label label-inverse-primary enable"
                                                                href='<?php echo base_url(); ?>administrator/transaksi/list/enable/<?php echo $row['kode_transaksi']; ?>?table=<?php echo base64_encode('tbl_transaksi'); ?>'>Accept/Kirim</a>
                                                            <?php } else if ($row['status'] == 2) { ?>
                                                            <a class="label label-inverse-primary enable"
                                                                href='#'>Selesai</a>
                                                            <?php } else { ?>
                                                            <a class="label label-inverse-warning desable"
                                                                href='#'>Pending</a>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-12">
                        <h2>Data Pemesanan</h2>
                        <div class="card">
                            <div class="card-block">
                                <div class="table-responsive dt-responsive">
                                    <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Tanggal Kunjungan</th>
                                                <th>Pasien</th>
                                                <th>Keluhan</th>
                                                <th>Ditangani</th>
                                                <th>Rekam Medis</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($kunjungan as $key => $row) : ?>
                                            <tr>
                                                <td><?php echo $key + 1; ?></td>
                                                <td><?php echo date("M d,Y", strtotime($row['tgl_kunjungan'])); ?></td>
                                                <td><?php echo $row['name']; ?></td>
                                                <td><a><?php echo $row['keluhan']; ?></a></td>
                                                <td><?php echo $row['nama_dokter']; ?></td>
                                                <td><a class="btn btn-primary"
                                                        href='<?php echo base_url(); ?>administrator/rekam-medis/kunjungan/<?php echo $row['id_rekam_medis']; ?>'>Rekam</a>
                                                </td>

                                                <td>
                                                    <?php if ($row['status'] == 1) { ?>
                                                    <a class="label label-inverse-primary enable"
                                                        href='<?php echo base_url(); ?>administrator/perawatan/enable/<?php echo $row['id_rekam_medis']; ?>?table=<?php echo base64_encode('tbl_rekam_medis'); ?>'>Enabled</a>
                                                    <?php } else { ?>
                                                    <a class="label label-inverse-warning desable"
                                                        href='<?php echo base_url(); ?>administrator/perawatan/disable/<?php echo $row['id_perawatan']; ?>?table=<?php echo base64_encode('tbl_perawatan'); ?>'>Disabled</a>
                                                    <?php } ?>
                                                    <a class="label label-inverse-info"
                                                        href='<?php echo base_url(); ?>administrator/perawatan/update-perawatan/<?php echo $row['id_perawatan']; ?>'>Edit</a>
                                                    <a class="label label-inverse-danger delete"
                                                        href='<?php echo base_url(); ?>administrator/perawatan/delete/<?php echo $row['id_perawatan']; ?>?table=<?php echo base64_encode('tbl_perawatan'); ?>'>Delete</a>

                                                </td>
                                            </tr>
                                            <?php endforeach; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>