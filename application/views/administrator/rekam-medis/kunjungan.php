<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/assets/pages/data-table/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/ekko-lightbox/dist/ekko-lightbox.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/lightbox2/dist/css/lightbox.css">



<div class="page-header">
    <div class="page-header-title">
        <h4>Rekam Medis - Kunjungan Pasien</h4>
    </div>
    <div class="page-header-breadcrumb">
        <ul class="breadcrumb-title">
            <li class="breadcrumb-item">
                <a href="index-2.html">
                    <i class="icofont icofont-home"></i>
                </a>
            </li>
            <li class="breadcrumb-item"><a href="#!">Rekam Medis</a>
            </li>
            <li class="breadcrumb-item"><a href="#!">Kunjungan</a>
            </li>
        </ul>
    </div>
</div>





<!-- Page-header end -->
<!-- Page-body start -->
<div class="page-body">
    <div>
        <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#formModalPerawatan">
            <i class="fas fa-plus"></i> Tambah Data Kunjungan
        </button>
    </div>
    <!-- DOM/Jquery table start -->

    <div class="card">
        <div class="card-block">
            <div class="table-responsive dt-responsive">
                <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tanggal Kunjungan</th>
                            <th>Pasien</th>
                            <th>Keluhan</th>
                            <th>Ditangani</th>
                            <th>Rekam Medis</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($kunjungan as $key => $row) : ?>
                        <tr>
                            <td><?php echo $key + 1; ?></td>
                            <td><?php echo date("M d,Y", strtotime($row['tgl_kunjungan'])); ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><a><?php echo $row['keluhan']; ?></a></td>
                            <td><?php echo $row['nama_dokter']; ?></td>
                            <td><a class="btn btn-primary"
                                    href='<?php echo base_url(); ?>administrator/rekam-medis/kunjungan/<?php echo $row['id_rekam_medis']; ?>'>Rekam</a>
                            </td>

                            <td>
                                <?php if ($row['status'] == 1) { ?>
                                <a class="label label-inverse-primary enable"
                                    href='<?php echo base_url(); ?>administrator/perawatan/enable/<?php echo $row['id_rekam_medis']; ?>?table=<?php echo base64_encode('tbl_rekam_medis'); ?>'>Enabled</a>
                                <?php } else { ?>
                                <a class="label label-inverse-warning desable"
                                    href='<?php echo base_url(); ?>administrator/perawatan/disable/<?php echo $row['id_perawatan']; ?>?table=<?php echo base64_encode('tbl_perawatan'); ?>'>Disabled</a>
                                <?php } ?>
                                <a class="label label-inverse-info"
                                    href='<?php echo base_url(); ?>administrator/perawatan/update-perawatan/<?php echo $row['id_perawatan']; ?>'>Edit</a>
                                <a class="label label-inverse-danger delete"
                                    href='<?php echo base_url(); ?>administrator/perawatan/delete/<?php echo $row['id_perawatan']; ?>?table=<?php echo base64_encode('tbl_perawatan'); ?>'>Delete</a>

                            </td>
                        </tr>
                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- DOM/Jquery table end -->
</div>

<!-- Modal -->
<div class="modal fade" id="formModalPerawatan" tabindex="-1" aria-labelledby="formModalLabelDokter" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabelDokter">Tambah Data Kunjungan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?php echo form_open_multipart('administrator/rekam-medis/kunjungan-add/add'); ?>

                <!-- select option -->
                <div class="form-group">
                    <label for="pasien">Pasien</label>
                    <select class="form-control" name="pasien" id="pasien" required>

                        <option value="">Pilih Pasien</option>
                        <?php foreach ($pasien as $nama) : ?>
                        <option value="<?php echo $nama['id']; ?>">
                            <?php echo $nama['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <!-- select option -->
                <div class="form-group">
                    <label for="dokter">Dokter / Perawat</label>
                    <select class="form-control" name="dokter" id="dokter" required>

                        <option value="">Pilih Dokter</option>
                        <?php foreach ($dokter as $dr) : ?>
                        <option value="<?php echo $dr['id']; ?>">
                            <?php echo $dr['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="tgl">Tanggal Kunjungan</label>
                    <input type="date" class="form-control" name="tgl_kunjungan" id="tgl_kunjungan" placeholder=""
                        value="<?= set_value('tanggal_kunjungan') ?>">
                    <small class="muted text-danger"><?= form_error('tanggal_kunjungan'); ?></small>
                </div>



                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-primary">Tambah</button>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>