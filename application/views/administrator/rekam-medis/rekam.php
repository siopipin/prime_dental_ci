<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/assets/pages/data-table/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/ekko-lightbox/dist/ekko-lightbox.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/lightbox2/dist/css/lightbox.css">



<div class="page-header">
    <div class="page-header-title">
        <h4>Rekam Kunjungan Pasien</h4>
    </div>
    <div class="page-header-breadcrumb">
        <ul class="breadcrumb-title">
            <li class="breadcrumb-item">
                <a href="index-2.html">
                    <i class="icofont icofont-home"></i>
                </a>
            </li>
            <li class="breadcrumb-item"><a href="#!">Rekam Medis</a>
            </li>
            <li class="breadcrumb-item"><a href="#!">Rekam</a>
            </li>
        </ul>
    </div>
</div>





<!-- Page-header end -->
<!-- Page-body start -->
<div class="page-body">
    <div>
        <p class="text-primary mb-2">
            <i class="fas fa-plus"></i> Data Kunjungan - <?= $datarekam['name']; ?>
        </p>
    </div>
    <!-- DOM/Jquery table start -->

    <div class="row">
        <!-- Data pasien -->
        <div class="col-md-6">
            <div class="col">
                <div class="card">
                    <div class="card-header bg-dark text-dark font-weight-bold text-center">Biodata Pasien</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Nama Pasien</th>
                                    <td><?= $datarekam['name']; ?></td>
                                </tr>
                                <tr>
                                    <th>Jenis Kelamin</th>
                                    <td><?= $datarekam['gender']; ?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal Lahir</th>
                                    <td><?= $datarekam['dob']; ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header bg-dark text-dark font-weight-bold text-center">Resep Obat / Produk</div>
                    <div class="card-body container">

                        <!-- aksi tambah transaksi -->
                        <?= form_open('administrator/rekam-medis/transaksi-add'); ?>
                        <input type="hidden" name="id_rekam_medis" value="<?= $datarekam['id_rekam_medis']; ?>">
                        <input type="hidden" name="id_user" value="<?= $datarekam['id_pasien']; ?>">
                        <!-- <input type="hidden" name="kode_transaksi" value="<?= $resepobats[0]['kode_transaksi']; ?>"> -->


                        <div class="row container">
                            <div class="form-group">
                                <label for="obat">Obat / Produk</label>
                                <select name="id_stok" required id="obat" class="form-control">
                                    <option value="">-- Pilih Obat --</option>
                                    <?php foreach ($obats as $o) : ?>
                                    <option value="<?= $o['id_stok']; ?>"><?= $o['nama']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="obat">Jumlah</label>
                                <input class="col-xl-5" type="number" name="jumlah" value="1"> </input>
                            </div>

                            <div class="form-group ml-2">
                                <button type="submit" class="btn btn-primary mt-4 "><i class="ti-plus"></i></button>
                            </div>

                            <?= form_close(); ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>No</th>
                                    <th>Nama Obat / Produk</th>
                                    <th><i class="fas fa-cogs"></i></th>
                                </tr>
                                <?php $no = 1;
                                foreach ($resepobats as $r) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $r['nama']; ?></td>
                                    <td>
                                        <a href="<?= base_url('administrator/rekam-medis/transaksi-hapus/' . $r['id_transaksi'] . '/' . $datarekam['id_rekam_medis']); ?>"
                                            class="btn btn-danger btn-sm" onclick="return confirm('Yakin ?')"><i
                                                class="ti-trash"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-dark text-dark font-weight-bold text-center">Informasi Rekam Medis</div>
                <div class="card-body container">
                    <!-- AKSI UNTUK SIMPAN REKAM -->
                    <?= form_open('administrator/rekam-medis/rekam-update'); ?>
                    <input type="hidden" name="id_rekam" required value="<?= $datarekam['id_rekam_medis']; ?>">

                    <div class="form-group">
                        <label for="keluhan">Keluhan</label>
                        <textarea name="keluhan" required id="keluhan"
                            class="form-control"><?= $datarekam['keluhan']; ?></textarea>
                        <small class="muted text-danger"><?= form_error('keluhan'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="diagnosa">Perawatan</label>
                        <select class="form-control" name="perawatan" required>
                            <option value="">Pilih Perawatan</option>
                            <?php foreach ($perawatan as $key => $rawat) : ?>
                            <option value="<?= $rawat['id_perawatan']; ?>"
                                <?php if ($rawat['id_perawatan'] == $datarekam['id_perawatan']) echo ' selected="selected"'; ?>>
                                <?= $rawat['nama']; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                        <small class="muted text-danger"><?= form_error('perawatan'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="penata">Penatalaksanaan</label>
                        <select name="penatalaksaan" id="penatalaksaan" required class="form-control">
                            <option value="">-- Pilih Penatalaksaan --</option>
                            <option value="Rawat Inap" <?php if ($datarekam['penatalaksaan'] == 'Rawat Inap') {
                                                            echo "selected";
                                                        } ?>>Rawat Inap</option>
                            <option value="Rawat Jalan" <?php if ($datarekam['penatalaksaan'] == 'Rawat Jalan') {
                                                            echo "selected";
                                                        } ?>>Rawat Jalan</option>
                            <option value="Rujuk" <?php if ($datarekam['penatalaksaan'] == 'Rujuk') {
                                                        echo "selected";
                                                    } ?>>Rujuk</option>
                            <option value="Lainnya" <?php if ($datarekam['penatalaksaan'] == 'Lainnya') {
                                                        echo "selected";
                                                    } ?>>Lainnya</option>
                        </select>
                        <small class="muted text-danger"><?= form_error('penata'); ?></small>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-dark">Simpan</button>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>

    <hr>
    <div>
        <p class="text-primary mb-2">
            <i class="fas fa-plus"></i> Riwayat Pasien - <?= $datarekam['name']; ?>
        </p>
    </div>
    <div class="row container">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-dark text-dark font-weight-bold text-center">Riwayat Kunjungan</div>
                <div class="card-body container">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tgl Berobat</th>
                                    <th>Keluhan</th>
                                    <th>Diagnosa</th>
                                    <th>Penatalaksanaan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($riwayatkunjungan as $r) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $r['tgl_kunjungan']; ?></td>
                                    <td><?= $r['keluhan']; ?></td>
                                    <td><?= $r['id_perawatan']; ?></td>
                                    <td><?= $r['penatalaksaan']; ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-dark text-dark font-weight-bold text-center">Riwayat Transaksi</div>
                <div class="card-body container">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tgl Transaksi</th>
                                    <th>Kode Trans.</th>
                                    <th>Produk</th>
                                    <th>Jenis</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($riwayattransaksi as $r) : ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $r['tgl_transaksi']; ?></td>
                                    <td><?= $r['kode_transaksi']; ?></td>
                                    <td><?= $r['nama']; ?></td>
                                    <td><?= $r['kategori']; ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!-- DOM/Jquery table end -->
</div>

<!-- Modal -->
<div class="modal fade" id="formModalPerawatan" tabindex="-1" aria-labelledby="formModalLabelDokter" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabelDokter">Tambah Data Kunjungan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?php echo form_open_multipart('administrator/rekam-medis/kunjungan-add/add'); ?>

                <!-- select option -->
                <div class="form-group">
                    <label for="pasien">Pasien</label>
                    <select class="form-control" name="pasien" id="pasien" required>

                        <option value="">Pilih Pasien</option>
                        <?php foreach ($pasien as $nama) : ?>
                        <option value="<?php echo $nama['id']; ?>">
                            <?php echo $nama['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <!-- select option -->
                <div class="form-group">
                    <label for="dokter">Dokter / Perawat</label>
                    <select class="form-control" name="dokter" id="dokter" required>

                        <option value="">Pilih Dokter</option>
                        <?php foreach ($dokter as $dr) : ?>
                        <option value="<?php echo $dr['id']; ?>">
                            <?php echo $dr['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="tgl">Tanggal Kunjungan</label>
                    <input type="date" class="form-control" name="tgl_kunjungan" id="tgl_kunjungan" placeholder=""
                        value="<?= set_value('tanggal_kunjungan') ?>">
                    <small class="muted text-danger"><?= form_error('tanggal_kunjungan'); ?></small>
                </div>



                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-primary">Tambah</button>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>