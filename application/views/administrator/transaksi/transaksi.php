<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/assets/pages/data-table/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/ekko-lightbox/dist/ekko-lightbox.css">
<link rel="stylesheet" type="text/css"
    href="<?php echo base_url(); ?>admintemplate/bower_components/lightbox2/dist/css/lightbox.css">


<div class="page-header">
    <div class="page-header-title">
        <h4>Transaksi</h4>
    </div>
    <div class="page-header-breadcrumb">
        <ul class="breadcrumb-title">
            <li class="breadcrumb-item">
                <a href="index-2.html">
                    <i class="icofont icofont-home"></i>
                </a>
            </li>
            <li class="breadcrumb-item"><a href="#!">Transaksi</a>
            </li>
            <li class="breadcrumb-item"><a href="#!">List</a>
            </li>
        </ul>
    </div>
</div>





<!-- Page-header end -->
<!-- Page-body start -->
<div class="page-body">

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-responsive dt-responsive">
                        <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Kode Transaksi</th>
                                    <th>Tgl</th>
                                    <th>img</th>
                                    <th>Nama Pembeli</th>
                                    <th>Produk</th>
                                    <th>Total</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($rows as $key => $row) : ?>
                                <tr>
                                    <td><?php echo $key + 1; ?></td>
                                    <td><?php echo $row['kode_transaksi']; ?></td>
                                    <td><?php echo date(
                                                'M d,Y H:i',
                                                strtotime($row['tgl_transaksi'])
                                            ); ?></td>

                                    <td>
                                        <img src="<?php echo base_url() .
                                                            'assets/images/bukti/' .
                                                            $row['img']; ?>" width="70px">
                                    </td>
                                    <td><?php echo $row['name']; ?></td>

                                    <td><?php echo $row['namastok']; ?></td>
                                    <td><?php echo $row['total']; ?></td>

                                    <td>
                                        <?php if ($row['status'] == 1) { ?>
                                        <a class="label label-inverse-primary enable"
                                            href='<?php echo base_url(); ?>administrator/transaksi/list/enable/<?php echo $row['kode_transaksi']; ?>?table=<?php echo base64_encode('tbl_transaksi'); ?>'>Accept/Kirim</a>
                                        <?php } else if ($row['status'] == 2) { ?>
                                        <a class="label label-inverse-primary enable" href='#'>Selesai</a>
                                        <?php } else { ?>
                                        <a class="label label-inverse-warning desable" href='#'>Pending</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- DOM/Jquery table end -->
</div>