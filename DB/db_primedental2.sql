-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 16, 2021 at 05:26 AM
-- Server version: 5.7.34
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_primedental`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_perawatan`
--

CREATE TABLE `tbl_jenis_perawatan` (
  `id_jenis` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `deskripsi` varchar(200) DEFAULT NULL,
  `icon` varchar(100) NOT NULL DEFAULT 'fa fa-ellipsis-h',
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_jenis_perawatan`
--

INSERT INTO `tbl_jenis_perawatan` (`id_jenis`, `nama`, `deskripsi`, `icon`, `update_at`) VALUES
(1, 'Perawatan Umum', 'Perawatan Umum', 'fa fa-pause', '2021-10-27 19:16:20'),
(2, 'Scalling', 'Pembersihan Karang Gigi', 'fa fa-sun-o', '2021-10-27 19:16:20'),
(3, 'Konservasi', 'Penambalan', 'fa fa-caret-square-o-up', '2021-10-27 19:16:23'),
(4, 'Ododentik', 'Perawatan Saluran Akar', 'fa fa-bullseye', '2021-10-27 19:16:23'),
(5, 'Pencabutan dan Bedah', 'Pencabutan dan Bedah Gigi atau Gusi', 'fa fa-dot-circle-o', '2021-10-27 19:18:39'),
(6, 'Protesa', 'Gigi Palsu', 'fa fa-exchange', '2021-10-27 19:19:24'),
(7, 'Gigi Palsu Lepasan', 'Gigi Palsu Lepasan', 'fa fa-ils', '2021-10-27 19:19:24'),
(8, 'Orthodontic', 'Merapikan Gigi', 'fa fa-th-large', '2021-10-27 19:20:24');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori_stok`
--

CREATE TABLE `tbl_kategori_stok` (
  `id_kategori_stok` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_kategori_stok`
--

INSERT INTO `tbl_kategori_stok` (`id_kategori_stok`, `nama`, `deskripsi`, `update_at`) VALUES
(1, 'Obat', 'Stok Obat', '2021-11-01 17:54:18'),
(2, 'Produk', 'Stok Produk', '2021-11-01 17:54:18'),
(3, 'Bahan', 'Stok Bahan Produk', '2021-11-01 17:54:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_perawatan`
--

CREATE TABLE `tbl_perawatan` (
  `id_perawatan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `deskripsi` varchar(300) NOT NULL,
  `biaya` varchar(12) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_perawatan`
--

INSERT INTO `tbl_perawatan` (`id_perawatan`, `id_jenis`, `nama`, `deskripsi`, `biaya`, `satuan`, `status`, `update_at`) VALUES
(1, 1, 'Konsultasi tanpa tindakan', '-', '20000', 'pcs', 1, '2021-10-27 19:22:02'),
(2, 1, 'Konsultasi dengan pemeriksaan', '-', '50000', 'pcs', 1, '2021-10-28 02:38:11'),
(3, 1, 'Foto Rontgent Per Kasus', 'Foto Rontgent Per Kasus', '150000', 'pcs', 1, '2021-10-28 02:39:06'),
(4, 1, 'Dental Scanning', '-', '500000', 'paket', 1, '2021-10-28 02:42:19'),
(5, 2, 'Scalling Class A, Sedikit', 'Scalling Class A, Sedikit2', '500000', 'pcs', 1, '2021-10-28 02:43:53'),
(7, 2, 'Scalling Class B, Sedang', 'Pembersihan Gigi Sedang2', '500000', 'pcs', 1, '2021-11-02 03:35:57'),
(8, 2, 'Scalling Class C, Banyak', 'Pembersihan disertai stain seluruh gigi', '650000', 'paket', 1, '2021-11-05 16:13:01'),
(9, 2, 'Office Bleacing', 'Bleaching', '3500000', 'paket', 1, '2021-11-05 16:14:14'),
(10, 2, 'Home Bleaching', 'Bleaching', '2000000', 'paket', 1, '2021-11-05 16:15:00'),
(11, 2, 'Dentova Mouth Wash', 'Pencuci Mulut', '35000', 'pcs', 1, '2021-11-05 16:15:32'),
(12, 3, 'GIC Class A, Kecil', 'Penambalan Kecil', '250000', 'pcs', 1, '2021-11-05 16:16:13'),
(13, 3, 'GIC Class B, Sedang', 'Penambalan Sedang', '300000', 'pcs', 1, '2021-11-05 16:17:25'),
(14, 3, 'GIC Class C,Besar', 'Penambalan Besar', '350000', 'pcs', 1, '2021-11-09 01:56:36'),
(15, 3, 'Composite Anterior (Gigi Depan)', 'Penambalan Gigi Depan', '500000', 'pcs', 1, '2021-11-09 01:58:26'),
(16, 3, 'Composite Class A, Kecil', 'Penambalan  Kecil Gigi Depan ', '300000', 'pcs', 1, '2021-11-09 02:02:00'),
(17, 3, 'Composite Class B, Sedang', 'Penambalan Sedang Gigi Depan', '350000', 'pcs', 1, '2021-11-09 02:03:17'),
(18, 3, 'Composite Class C, Besar', 'Penambalan Besar Gigi Depan ', '400000', 'pcs', 1, '2021-11-09 02:04:47'),
(19, 3, 'Pemberian Obat Gigi + TS (Visit 1 PSA)', 'Obat Gigi Penambalan', '300000', 'pcs', 1, '2021-11-09 02:07:48'),
(20, 3, 'PIT & Fissure Sealant', 'Pencegahan Gigi Karies/berlobang', '200000', 'pcs', 1, '2021-11-09 02:15:57'),
(21, 3, 'Topikal Fluoridasi / Ml Varnish', 'Perawatan Gigi pada Anak', '500000', 'paket', 1, '2021-11-09 02:19:09'),
(22, 3, 'ML Varnish + Scaling', 'Pembersihan Karang Gigi', '700000', 'paket', 1, '2021-11-09 02:22:35'),
(23, 4, 'Anterior (Gigi Depan I-C)', 'Perawatan Gigi depan', '1000000', 'paket', 1, '2021-11-09 02:28:13'),
(24, 4, 'Premolar', 'Perawatan Graham Kecil', '1300000', 'paket', 1, '2021-11-09 02:32:00'),
(25, 4, 'Molar Bawah', 'Perawatan Graham Bawah', '1500000', 'paket', 1, '2021-11-09 02:34:44'),
(26, 4, 'Molar Atas', 'Perawatan Graham Atas', '2000000', 'paket', 1, '2021-11-09 02:35:52'),
(27, 4, 'Retreatment Anterior', 'Perawatan Anterior', '1500000', 'paket', 1, '2021-11-09 02:41:46'),
(28, 4, 'Retreatment  Premolar', 'Perawatan Premolar', '1700000', 'paket', 1, '2021-11-09 02:43:20'),
(29, 4, 'Retreatment  Molar Bawah', 'Perawatan Molar Bawah', '2000000', 'paket', 1, '2021-11-09 02:44:13'),
(30, 4, 'Retreatment  Molar Atas', 'Perawatan Molar Atas', '2500000', 'paket', 1, '2021-11-09 02:45:04'),
(31, 4, 'Premedikasi', 'Obat Premedikasi', '100000', 'pcs', 1, '2021-11-09 02:48:20'),
(32, 4, 'Pedo RCT', 'Perawatan saluran akar pada anak', '700000', 'pcs', 1, '2021-11-09 02:52:12'),
(33, 5, 'Pencabutan Tanpa Anestesi', 'Pencabutan Gigi ', '200000', 'pcs', 1, '2021-11-09 02:53:18'),
(34, 5, 'Pencabutan Dengan Anestesi', 'Pencabutan Gigi Anak dengan Anestesi', '250000', 'pcs', 1, '2021-11-09 02:54:45'),
(35, 5, 'Class A', 'Pencabutan Class A', '350000', 'pcs', 1, '2021-11-09 02:56:15'),
(36, 5, 'Class B', 'Pencabutan Class B', '500000', 'pcs', 1, '2021-11-09 02:56:53'),
(37, 5, 'Class C', 'Komplikasi  gigi ', '600000', 'pcs', 1, '2021-11-09 02:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rekam_medis`
--

CREATE TABLE `tbl_rekam_medis` (
  `id_rekam_medis` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `tgl_kunjungan` date NOT NULL,
  `keluhan` text,
  `id_perawatan` int(11) NOT NULL,
  `penatalaksaan` enum('Rawat Jalan','Rawat Inap','Rujuk','Lainnya') DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `updated at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_rekam_medis`
--

INSERT INTO `tbl_rekam_medis` (`id_rekam_medis`, `id_pasien`, `id_dokter`, `tgl_kunjungan`, `keluhan`, `id_perawatan`, `penatalaksaan`, `status`, `updated at`) VALUES
(1, 23, 24, '2020-10-17', 'Sakit Gigi dan gigi berlubang', 33, 'Rawat Jalan', 1, '2021-11-09 04:59:11'),
(3, 23, 24, '2020-10-21', 'Bau nafas dan karang gigi', 1, 'Rujuk', 1, '2021-11-09 04:59:11'),
(4, 21, 24, '2021-10-15', 'Lubang pada gigi', 3, 'Rawat Jalan', 1, '2021-11-09 04:59:11'),
(5, 21, 24, '2021-11-05', NULL, 4, NULL, 1, '2021-11-09 05:25:55'),
(6, 22, 24, '2021-11-04', NULL, 5, NULL, 1, '2021-11-09 15:21:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_riwayat_stok`
--

CREATE TABLE `tbl_riwayat_stok` (
  `id_riwayat_stok` int(11) NOT NULL,
  `sku` varchar(6) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `stok` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '1',
  `id_transaksi` varchar(10) DEFAULT NULL,
  `tgl_transaksi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_riwayat_stok`
--

INSERT INTO `tbl_riwayat_stok` (`id_riwayat_stok`, `sku`, `status`, `stok`, `id_user`, `id_transaksi`, `tgl_transaksi`, `updated_at`, `notes`) VALUES
(1, 'OB01', 0, 3, 1, NULL, '2021-11-01 00:00:00', '2021-11-07 20:07:21', ''),
(2, 'OB02', 0, 6, 1, NULL, '2021-11-01 00:00:00', '2021-11-07 20:08:05', ''),
(3, 'OB02', 0, 1, 1, '-', '2021-11-08 05:09:40', '2021-11-07 22:09:40', 'Obat baru'),
(5, 'OB01', 0, 1, 1, '-', '2021-11-08 05:33:02', '2021-11-07 22:33:02', ''),
(6, 'OB01', 0, 1, 1, '-', '2021-11-08 05:35:57', '2021-11-07 22:35:57', ''),
(7, 'OB02', 0, 1, 1, '-', '2021-11-08 09:02:54', '2021-11-08 02:02:54', ''),
(8, 'OB02', 0, 2, 1, '-', '2021-11-08 09:03:39', '2021-11-08 02:03:39', ''),
(9, 'OB01', 0, 10, 1, '-', '2021-11-08 09:05:58', '2021-11-08 02:05:58', ''),
(10, 'OB01', 0, 1, 1, '-', '2021-11-08 09:07:00', '2021-11-08 02:07:00', ''),
(11, 'OB01', 0, 1, 1, '-', '2021-11-08 09:08:55', '2021-11-08 02:08:55', ''),
(12, 'OB01', 0, 2, 1, '-', '2021-11-08 09:14:36', '2021-11-08 02:14:36', ''),
(13, 'OB01', 0, 1, 1, '-', '2021-11-08 09:15:23', '2021-11-08 02:15:23', ''),
(14, 'OB01', 0, 2, 1, '-', '2021-11-08 09:19:04', '2021-11-08 02:19:04', ''),
(15, 'OB01', 1, 2, 1, '-', '2021-11-08 09:19:36', '2021-11-08 02:19:36', ''),
(16, 'OB01', 2, 2, 1, '-', '2021-11-08 09:19:42', '2021-11-08 02:19:42', ''),
(17, 'OB01', 0, 2, 1, '-', '2021-11-08 09:20:25', '2021-11-08 02:20:25', ''),
(18, 'OB01', 0, 2, 1, '-', '2021-11-08 09:22:39', '2021-11-08 02:22:39', ''),
(19, 'OB01', 0, 2, 1, '-', '2021-11-08 09:23:24', '2021-11-08 02:23:24', ''),
(20, 'OB01', 0, 2, 1, '-', '2021-11-08 09:24:17', '2021-11-08 02:24:17', ''),
(21, 'OB01', 0, 2, 1, '-', '2021-11-08 09:28:29', '2021-11-08 02:28:29', ''),
(22, 'OB01', 0, 1, 1, '-', '2021-11-08 09:28:52', '2021-11-08 02:28:52', ''),
(23, 'OB01', 0, 1, 1, '-', '2021-11-08 09:29:12', '2021-11-08 02:29:12', ''),
(24, 'OB01', 0, 2, 1, '-', '2021-11-08 09:29:33', '2021-11-08 02:29:33', ''),
(25, 'OB01', 0, 2, 1, '-', '2021-11-08 09:29:57', '2021-11-08 02:29:57', ''),
(26, 'OB01', 1, 2, 1, '-', '2021-11-08 09:35:27', '2021-11-08 02:35:27', ''),
(27, 'OB01', 0, 2, 1, '-', '2021-11-08 09:46:31', '2021-11-08 02:46:31', ''),
(28, 'OB01', 1, 4, 1, 'ID001', '2021-11-08 09:47:04', '2021-11-08 02:47:04', 'Beli'),
(29, 'OB01', 0, 10, 1, '-', '2021-11-09 11:06:50', '2021-11-09 04:06:50', ''),
(30, 'OB02', 0, 12, 1, '-', '2021-11-09 11:13:00', '2021-11-09 04:13:00', ''),
(31, 'OB04', 0, 10, 1, '-', '2021-11-09 11:24:14', '2021-11-09 04:24:14', ''),
(32, 'BO03', 0, 3, 1, '-', '2021-11-10 10:30:29', '2021-11-10 03:30:29', 'barang baru'),
(33, 'BO02', 0, 1, 1, '-', '2021-11-10 10:34:52', '2021-11-10 03:34:52', ''),
(34, 'OB02', 0, 10, 1, '-', '2021-11-10 10:35:24', '2021-11-10 03:35:24', ''),
(35, 'PB09', 0, 12, 1, '-', '2021-11-10 11:22:33', '2021-11-10 04:22:33', ''),
(36, 'PB07', 0, 12, 1, '-', '2021-11-10 11:23:37', '2021-11-10 04:23:37', ''),
(37, 'OB030', 0, 2, 1, '-', '2021-11-16 12:20:33', '2021-11-16 05:20:33', 'barang baru');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stok`
--

CREATE TABLE `tbl_stok` (
  `id_stok` int(11) NOT NULL,
  `sku` varchar(6) NOT NULL,
  `id_jenis` int(11) NOT NULL DEFAULT '1',
  `nama` varchar(200) NOT NULL,
  `stok` int(11) NOT NULL,
  `deskripsi` varchar(3000) DEFAULT NULL,
  `satuan` varchar(10) DEFAULT 'pcs',
  `biaya` varchar(12) NOT NULL DEFAULT '0',
  `tanggal_masuk` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT '1',
  `img` varchar(200) DEFAULT 'produk.jpg',
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_stok`
--

INSERT INTO `tbl_stok` (`id_stok`, `sku`, `id_jenis`, `nama`, `stok`, `deskripsi`, `satuan`, `biaya`, `tanggal_masuk`, `status`, `img`, `update_at`) VALUES
(1, 'OB01', 1, 'Ponstan', 11, 'Ponstan sering digunakan untuk mengobati rasa sakit mulai dari sakit gigi sampai cedera otot bahkan sakit setelah melahirkan.', 'pcs', '320000', '2021-10-04 17:00:00', 1, 'Ponstan-300x300.jpg', '2021-11-01 18:26:03'),
(2, 'OB02', 1, 'Ibuprofen', 30, 'Ibuprofen merupakan golongan obat-obatan yang mampu meringankan rasa sakit dan meredakan inflamasi atau peradangan.', 'pcs', '210000', '2021-10-31 17:00:00', 1, 'Ibuprofen.jpg', '2021-11-05 18:28:02'),
(3, 'OB03', 1, 'Dentasol', 2, 'Dalam obat ini terdapat zat anestesi topikal dengan antiseptik yang dapat diaplikasikan secara langsung pada gigi yang sakit.\n\nKandungan zat tersebut mampu mengurangi rasa nyeri akibat sakit gigi juga mampu mengobati infeksi jamur dan bakteri lho, Toppers.', 'pcs', '45000', '2021-11-05 17:00:00', 1, 'Dentasol.jpg', '2021-11-06 00:52:38'),
(13, 'OB04', 1, 'Obat Sakit Gigi Cap Burung Kakak Tua', 0, 'Obat sakit gigi Cap Burung Kakak Tua ini mengandung senyawa gliserin, ethanol, creosote, dan oleum caryophylli.  Kandungan tersebut berguna untuk mengatasi sakit gigi, gusi bengkak, dan membersihkan gigi dari kuman yang menempel.  Untuk menggunakan obat sakit gigi bisa langsung ditempelkan menggunakan kapas atau dengan cara dikumur. ', 'pcs', '250000', '2021-11-02 17:00:00', 1, 'burungkaka.jpg', '2021-11-08 19:31:05'),
(14, 'PB01', 2, 'Veneer', 0, 'veneer adalah lapisan bahan yang ditempatkan di atas gigi', 'pcs', '1000000', '2021-11-08 20:12:34', 1, 'veener.jpg', '2021-11-08 20:12:34'),
(15, 'BO01', 3, 'Zirconia', 0, 'Zirconium merupakan logam yang bewarna putih keabuan dan sangat jarang ditemukan di alam bebas', 'pcs', '980000', '2021-11-08 20:17:20', 1, 'zirconia.jpg', '2021-11-08 20:17:20'),
(16, 'BO02', 3, 'Porcelain', 1, 'Bahan yang digunakan Full porcelain / Emax bahan khusus untuk membuat gigi tiruan permanent.', 'pcs', '560000', '2021-11-08 20:19:52', 1, 'porcelain.jpeg', '2021-11-08 20:19:52'),
(17, 'OB05', 1, 'Super Green Plus SG', 10, '-', 'pcs', '50000', '2021-11-09 03:06:24', 1, '1.jpg', '2021-11-09 03:06:24'),
(18, 'OB06', 1, 'ProSagi', 0, '-', 'pcs', '16500', '2021-11-09 04:12:12', 1, '2.jpg', '2021-11-09 04:12:12'),
(19, 'OB07', 1, 'Hersagi', 0, '-', 'paket', '60000', '2021-11-09 04:17:28', 1, '3.jpg', '2021-11-09 04:17:28'),
(20, 'OB08', 1, 'Zentri', 0, '-', 'pcs', '75000', '2021-11-09 04:19:12', 1, 'download.jpg', '2021-11-09 04:19:12'),
(21, 'OB09', 1, 'Penisilin', 0, 'Obat Antibiotik', 'paket', '25000', '2021-11-09 04:28:32', 1, 'download_(3).jpg', '2021-11-09 04:28:32'),
(22, 'OB10', 1, 'Paracetamol', -12, '', 'paket', '15000', '2021-11-09 04:30:06', 1, 'download_(1).jpg', '2021-11-09 04:30:06'),
(23, 'OB11', 1, 'Naproxen', 5, '', 'paket', '15000', '2021-11-09 04:33:50', 1, 'Naproxen.jpg', '2021-11-09 04:33:50'),
(24, 'OB12', 1, 'Clindamycin', 2, '', 'paket', '30000', '2021-11-09 04:38:13', 1, 'download_(5).jpg', '2021-11-09 04:38:13'),
(25, 'PB02', 2, 'Crown dan Bridge', 0, 'Jenis gigi tiruan/palsu yang tidak dapat dilepas pasang (permanen)', 'pcs', '1000000', '2021-11-09 04:47:38', 1, 'g1.jpg', '2021-11-09 04:47:38'),
(26, 'PB03', 2, 'Build Up', 0, '', 'pcs', '250000', '2021-11-09 04:56:38', 1, 'download_(7).jpg', '2021-11-09 04:56:38'),
(27, 'PB04', 2, 'Wing/Rest', 0, 'Gigi tiuran yang dibut tanpa gusi tiruan dibagian bukal/labial', 'pcs', '200000', '2021-11-09 05:04:08', 1, 'download_(8).jpg', '2021-11-09 05:04:08'),
(28, 'PB05', 2, 'Wax Up', 0, 'Gigi yang dimodifikasi sesuai dengan bentuk gigi yang asli', 'pcs', '1000000', '2021-11-09 05:11:53', 1, 'download_(9).jpg', '2021-11-09 05:11:53'),
(29, 'PB06', 2, 'Coping', 0, 'Jenis gigi tiruan yang berbahan logam yang berwarna gelap', 'pcs', '1200000', '2021-11-09 06:04:24', 1, 'download_(10).jpg', '2021-11-09 06:04:24'),
(30, 'PB07', 2, 'Redo', 12, 'Perawatan pada gigi palsu yang sering renggang', 'pcs', '100000', '2021-11-09 06:12:19', 1, 'download_(11).jpg', '2021-11-09 06:12:19'),
(31, 'PB08', 2, 'Implant Processing', 0, 'Penanaman akar gigi buatan seperti baut pada rahang yang terbuat dari logam khusus', 'pcs', '1000000', '2021-11-09 06:16:59', 1, 'download_(12).jpg', '2021-11-09 06:16:59'),
(32, 'PB09', 2, '3D Printing', 12, 'Gigi palsu dan kawat gigi yang pas dan cocok untuk pasien', 'pcs', '300000', '2021-11-09 06:26:46', 1, 'download_(13).jpg', '2021-11-09 06:26:46'),
(33, 'PB10', 2, 'Extra Revision', 0, 'perawatan tambahan pada gigi', 'pcs', '100000', '2021-11-09 06:30:34', 1, 'download_(14).jpg', '2021-11-09 06:30:34'),
(34, 'BO03', 3, 'LD Press', -3, 'Penambalan gigi yang berlubang', 'pcs', '200000', '2021-11-09 06:35:19', 1, 'download_(15).jpg', '2021-11-09 06:35:19'),
(35, 'BO04', 3, 'Resin', 0, 'Bahan tumputan gigi serta memodifikasi warna gigi dan kontur gigi', 'paket', '1000000', '2021-11-09 06:38:42', 1, 'images.jpg', '2021-11-09 06:38:42'),
(36, 'BO06', 3, 'Wax', 0, 'Bahan yang digunakan untuk membuat lempengan pada sasar gigi palsu ', 'pcs', '500000', '2021-11-09 06:41:37', 1, 'images_(1).jpg', '2021-11-09 06:41:37'),
(37, 'BO07', 3, 'Feldspathic', 0, 'Bahan untuk rekontruksi gigi yang tersusun dari kaca ataupun kristal', 'pcs', '300000', '2021-11-09 06:45:41', 1, 'download_(16).jpg', '2021-11-09 06:45:41'),
(38, 'OB030', 1, 'Obat Sakit Gigi2', 2, 'Obat sakit gigi Cap Burung Kakak Tua ini mengandung senyawa gliserin, ethanol, creosote, dan oleum caryophylli.  Kandungan tersebut berguna untuk mengatasi sakit gigi, gusi bengkak, dan membersihkan gigi dari kuman yang menempel.  Untuk menggunakan obat sakit gigi bisa langsung ditempelkan menggunakan kapas atau dengan cara dikumur. ', 'pcs', '23000', '2021-11-16 05:19:47', 1, 'Ponstan-300x300.jpg', '2021-11-16 05:19:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `kode_transaksi` varchar(10) DEFAULT NULL,
  `nama_penerima` varchar(100) DEFAULT NULL,
  `alamat_penerima` varchar(300) DEFAULT NULL,
  `hp_penerima` varchar(20) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_rekam_medis` int(11) NOT NULL,
  `id_stok` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `img` varchar(200) DEFAULT NULL,
  `pengirim` varchar(100) NOT NULL,
  `tgl_transaksi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `jumlah` int(11) NOT NULL,
  `total` varchar(12) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`id_transaksi`, `kode_transaksi`, `nama_penerima`, `alamat_penerima`, `hp_penerima`, `id_user`, `id_rekam_medis`, `id_stok`, `status`, `img`, `pengirim`, `tgl_transaksi`, `jumlah`, `total`, `updated_at`) VALUES
(1, 'TRS0211', NULL, NULL, NULL, 23, 1, 1, 0, NULL, '', '2021-11-09 09:33:37', 1, '450000', '2021-11-09 09:33:37'),
(3, 'TRS0211', NULL, NULL, NULL, 23, 1, 3, 0, NULL, '', '2021-11-09 22:09:06', 2, '0', '2021-11-09 22:09:06'),
(4, 'TRS0211', NULL, NULL, NULL, 23, 1, 24, 0, NULL, '', '2021-11-10 09:06:02', 1, '0', '2021-11-10 09:06:02'),
(5, 'TRS278', NULL, NULL, NULL, 23, 3, 2, 0, NULL, '', '2021-11-10 10:59:35', 1, '0', '2021-11-10 10:59:35'),
(24, 'TRS2396', 'Citra Eka', 'Medan', '099888', 23, 0, 37, 2, 'buktitf.jpeg', 'Andi', '2021-11-16 05:04:21', 1, '300000', '2021-11-16 05:04:21'),
(25, 'TRS1393', 'Poli', 'Jalanan', '0988999', 1, 0, 35, 0, NULL, '', '2021-11-16 08:27:11', 1, '1000000', '2021-11-16 08:27:11'),
(26, 'TRS1393', 'Poli', 'Jalanan', '0988999', 1, 0, 37, 0, NULL, '', '2021-11-16 08:27:11', 4, '300000', '2021-11-16 08:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `golongan_darah` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `username`, `contact`, `address`, `gender`, `image`, `role_id`, `golongan_darah`, `dob`, `status`, `register_date`) VALUES
(1, 'Citra', 'citra@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'citra', '082276438906', 'Jl Gelas 12A Medan', 'Female', 'YADU_Logo.JPG', 1, 'O', '1998-08-03', 1, '2021-10-27 09:44:43'),
(4, 'drg. Kelvin Tantono', 'kelvin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'kelvin', '9898989898', 'Medan', 'Male', 'slide_05.jpg', 2, 'O', '1990-08-03', 1, '2019-08-13 18:49:15'),
(21, 'budi', 'budi@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'budi', '0999', 'jalan', 'Male', 'ktp_cover.jpeg', 4, 'A', '2021-10-08', 1, '2021-10-27 11:06:56'),
(22, 'Setriani', 'setriani@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'setriani', '09888', 'Medan', 'Male', 'noimage.jpg', 4, 'O', '1999-02-02', 1, '2021-11-08 13:23:01'),
(23, 'deswenti', 'deswenti@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'deswenti', '0992889', 'Medan', 'Female', 'user.jpeg', 4, 'A', '1992-05-05', 1, '2021-11-08 13:28:47'),
(24, 'drg. Fenny Valentine Gouw', 'fenny@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'fenny', '09988789', 'Medan', 'Female', 'fenny.jpg', 3, 'O', '1993-02-03', 1, '2021-11-08 13:33:34'),
(25, 'yanto', 'yanto@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'yanto', '', '', '', '', 4, NULL, '', 0, '2021-11-09 15:35:49'),
(28, 'krina', 'krina@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'krina', '', '', '', '', 4, NULL, '', 0, '2021-11-10 02:29:25'),
(29, 'andi', 'andi@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'andi', '', '', '', '', 4, NULL, '', 0, '2021-11-10 02:31:17'),
(30, 'ani', 'ani@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'ani', '', '', '', '', 4, NULL, '', 0, '2021-11-10 02:32:27'),
(31, 'andika', 'andika@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'andika', '', '', '', '', 4, NULL, '', 0, '2021-11-10 06:11:00'),
(32, 'abu', 'abu@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'abu', '', '', '', '', 4, NULL, '', 0, '2021-11-16 05:21:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_jenis_perawatan`
--
ALTER TABLE `tbl_jenis_perawatan`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `tbl_kategori_stok`
--
ALTER TABLE `tbl_kategori_stok`
  ADD PRIMARY KEY (`id_kategori_stok`);

--
-- Indexes for table `tbl_perawatan`
--
ALTER TABLE `tbl_perawatan`
  ADD PRIMARY KEY (`id_perawatan`);

--
-- Indexes for table `tbl_rekam_medis`
--
ALTER TABLE `tbl_rekam_medis`
  ADD PRIMARY KEY (`id_rekam_medis`),
  ADD KEY `id_dokter` (`id_dokter`),
  ADD KEY `id_pasien` (`id_pasien`);

--
-- Indexes for table `tbl_riwayat_stok`
--
ALTER TABLE `tbl_riwayat_stok`
  ADD PRIMARY KEY (`id_riwayat_stok`);

--
-- Indexes for table `tbl_stok`
--
ALTER TABLE `tbl_stok`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indexes for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_jenis_perawatan`
--
ALTER TABLE `tbl_jenis_perawatan`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_kategori_stok`
--
ALTER TABLE `tbl_kategori_stok`
  MODIFY `id_kategori_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_perawatan`
--
ALTER TABLE `tbl_perawatan`
  MODIFY `id_perawatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tbl_rekam_medis`
--
ALTER TABLE `tbl_rekam_medis`
  MODIFY `id_rekam_medis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_riwayat_stok`
--
ALTER TABLE `tbl_riwayat_stok`
  MODIFY `id_riwayat_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tbl_stok`
--
ALTER TABLE `tbl_stok`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
