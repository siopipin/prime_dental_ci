-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 09, 2021 at 01:21 AM
-- Server version: 5.7.34
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_primedental`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `user_id`, `type`, `status`, `created_at`) VALUES
(1, 'Technical', 1, 'blog', 0, '2017-07-31 15:03:14'),
(2, 'Business', 1, 'blog', 0, '2017-07-31 15:03:14'),
(4, 'T-Shirts', 3, 'product', 1, '2017-08-10 14:49:47'),
(5, 'Shirts', 3, 'product', 1, '2017-08-10 14:51:38'),
(6, 'FAQ Category Onee', 1, 'faq', 1, '2017-08-14 15:00:07'),
(7, 'FAQ Category two', 1, 'faq', 1, '2017-08-14 15:11:39');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `comment_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `username`, `email`, `comment`, `comment_type`, `status`, `created_at`) VALUES
(1, 4, 'Astha Sharma', 'astha@yopmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a lobortis metus. Mauris eget turpis consectetur, gravida elit ac, gravida nibh. Maecenas massa eros, finibus id leo vitae, tempor tristique elit. Vestibulum vel nunc porttitor, feugiat quam nec, luctus justo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ullamcorper at tellus non elementum. Fusce vestibulum magna odio, eget efficitur lacus mattis ut. Integer lacinia tortor id mattis porta. ', 'blog', 1, '2017-08-01 18:35:45');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `faq_cat_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` longtext NOT NULL,
  `datetime` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `faq_cat_id`, `question`, `answer`, `datetime`, `status`) VALUES
(1, 7, 'How to uses', '<p>go to registration page and registred yourself...</p>\r\n', '2017-08-14 21:04:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `file_name`, `created_at`) VALUES
(39, 'gallery', '19120392_488748308124181_1356232594835898368_n.jpg', '0000-00-00 00:00:00'),
(40, 'gallery', '20066031_249507905540880_3425795962604355584_n.jpg', '0000-00-00 00:00:00'),
(41, 'gallery', '15877061_119553495224327_312221402042204160_n.jpg', '0000-00-00 00:00:00'),
(42, 'gallery', '16123278_175559819590919_7196532057399361536_n.jpg', '0000-00-00 00:00:00'),
(43, 'gallery', '16583116_1739753663021127_1111287715137060864_n.jpg', '0000-00-00 00:00:00'),
(44, 'gallery', '14574113_652781028238295_278229033701867520_n.jpg', '0000-00-00 00:00:00'),
(45, 'gallery', '15403505_270429253371351_6273410913949188096_n.jpg', '0000-00-00 00:00:00'),
(46, 'gallery', '15538121_1018367098290603_1806110868400766976_n.jpg', '0000-00-00 00:00:00'),
(47, 'gallery', '15538471_145984329220644_613923239470563328_n.jpg', '0000-00-00 00:00:00'),
(48, 'gallery', '15538718_217169452072899_4305560102691143680_n.jpg', '0000-00-00 00:00:00'),
(49, 'gallery', '15538789_241209666312136_5682089248384090112_n.jpg', '0000-00-00 00:00:00'),
(50, 'gallery', '15623741_814558755361976_817742299776679936_n.jpg', '0000-00-00 00:00:00'),
(51, 'gallery', '15625408_1148559301925971_939940647698169856_n.jpg', '0000-00-00 00:00:00'),
(53, 'gallery', '15802415_1849186702026963_3549623829707882496_n.jpg', '0000-00-00 00:00:00'),
(54, 'gallery', '16908777_182954125530306_8770190177525039104_n.jpg', '0000-00-00 00:00:00'),
(55, 'gallery', '17076922_1397885136900295_4402714397525409792_n.jpg', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `page_content`
--

CREATE TABLE `page_content` (
  `id` int(11) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `datetime` datetime NOT NULL,
  `updated_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_content`
--

INSERT INTO `page_content` (`id`, `page_name`, `content`, `datetime`, `updated_datetime`) VALUES
(1, 'About-Us', '<p>skdjshds dusdj sd sjd</p>\r\n', '0000-00-00 00:00:00', '2017-08-16 18:03:05'),
(2, 'Term And Condition', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Privacy policy', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Contact-Us', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `category_id`, `user_id`, `post_image`, `body`, `status`, `created_at`) VALUES
(4, 'Lorem Ipsum Blog', 'lorem-ipsum-blog', 2, 2, 'minions01.jpg', '<p><em><strong>Lorem Ipsum</strong></em> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 1, '2017-08-01 15:51:21'),
(5, 'Blog Post Four', 'blog-post-four', 1, 1, 'Hydrangeas.jpg', '<p><span style=\"font-family:Courier New,Courier,monospace\">Curabitur molestie justo eget augue fringilla, vitae finibus enim euismod. Aenean commodo urna turpis, sed cursus est egestas at. Ut ut convallis enim. Donec elementum, sem ac consequat iaculis, eros dolor porta justo, eu faucibus est nunc ut risus. Sed vulputate elementum elit vel posuere. Nunc vel aliquam enim. Curabitur viverra, velit eu sagittis vulputate, libero eros varius libero, eget tristique felis neque eget erat. Maecenas condimentum commodo nunc, eget malesuada lorem imperdiet in. Phasellus imperdiet tempor arcu, et hendrerit orci malesuada sed. Vivamus ultricies suscipit faucibus. Suspendisse sollicitudin quis ipsum at semper. Donec quis augue vel nibh condimentum laoreet eu ac sapien. Pellentesque ex enim, placerat et porta sit amet, finibus vel felis. Vivamus mollis euismod nisl, at accumsan leo. Ut fermentum lacus quis sagittis elementum. Aenean velit nunc, aliquet a lorem vestibulum, consectetur malesuada felis. </span></p>\r\n', 1, '2017-08-03 14:25:28'),
(6, 'Lorem Ipsum Blog Post', 'lorem-ipsum-blog-post', 3, 1, 'Jellyfish.jpg', '<p><em>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc</em>.</p>\r\n', 1, '2017-08-03 15:56:07'),
(7, 'Blog Post New', 'blog-post-new', 1, 2, 'noimage.jpg', '<p><span style=\"font-family:Times New Roman,Times,serif\"><em>Aliquam sed ornare dui. In sollicitudin dolor a varius venenatis. Nam eu purus id felis venenatis dictum sit amet eu neque. Sed congue tortor egestas, volutpat nisl ac, congue nunc. Fusce ut euismod massa, ornare scelerisque dui. Mauris vitae lectus eu lacus sagittis sodales at et enim. Suspendisse nisl lacus, imperdiet eget congue ut, sodales vel ex. Nam ut faucibus dui, vel fringilla leo. Duis laoreet, ex non ultricies molestie, justo erat scelerisque mauris, ut tristique sapien nisi sit amet velit. Quisque pulvinar lectus odio. Donec a nulla sed dui blandit ultrices eu et libero.</em></span></p>\r\n', 1, '2017-08-03 15:59:09');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `save_price` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `img_alt_tag` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `tag` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `datetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_tag` varchar(255) NOT NULL,
  `meta_desc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `cat_id`, `sku`, `name`, `quantity`, `price`, `save_price`, `color`, `image`, `img_alt_tag`, `short_description`, `description`, `tag`, `size`, `datetime`, `status`, `meta_title`, `meta_tag`, `meta_desc`) VALUES
(1, 1, 0, 'GSS34523', '', '', '', '', '', '', '', '', '', '', '', '2017-08-14 20:51:39', 0, '', '', ''),
(2, 1, 5, 'ABC123', 'Dressing table', '12', '2999', '1', 'RED', 'bd2_6f5_636_330-1-original.jpg', '', 'Dressing table', '<p>Dressing table</p>\r\n', 'Woodland White Tshirts', 'medium', '2017-08-16 23:33:26', 1, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `file_name`) VALUES
(6, 1, 'tshirts2.jpg'),
(7, 1, 'tshirt.jpg'),
(8, 1, 'imagesaaaa.jpg'),
(9, 1, 'imagesaa.jpg'),
(10, 2, 'bd2_6f5_636_330-1-original.jpg'),
(11, 2, 'command.png'),
(12, 2, 'yyyy.jpg'),
(13, 2, 'yy.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sco`
--

CREATE TABLE `sco` (
  `id` int(11) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sco`
--

INSERT INTO `sco` (`id`, `page_name`, `title`, `keywords`, `description`) VALUES
(1, 'Home', 'Home', '', 'Homess'),
(2, 'About-Us', 'About-Us', '', ''),
(3, 'Gallery', '', '', ''),
(4, 'Contact-Us', '', '', ''),
(5, 'Term And Condition', '', '', ''),
(6, 'Privacy Policy', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `site_config`
--

CREATE TABLE `site_config` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `logo_img` varchar(255) NOT NULL,
  `site_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_config`
--

INSERT INTO `site_config` (`id`, `site_name`, `logo_img`, `site_title`) VALUES
(1, 'E commerce Site', 'yadi-ci-logo.png', 'E commerce Site');

-- --------------------------------------------------------

--
-- Table structure for table `sliders_img`
--

CREATE TABLE `sliders_img` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders_img`
--

INSERT INTO `sliders_img` (`id`, `title`, `description`, `image`, `status`) VALUES
(1, 'A Furniture Shops', 'A Furniture Shops', 'YADU_Logo.JPG', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sociallinks`
--

CREATE TABLE `sociallinks` (
  `id` int(11) NOT NULL,
  `social_name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sociallinks`
--

INSERT INTO `sociallinks` (`id`, `social_name`, `link`) VALUES
(1, 'Facebook', ''),
(2, 'Twitter', ''),
(3, 'Instagram', ''),
(4, 'Linkdin', ''),
(5, 'Skype', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_perawatan`
--

CREATE TABLE `tbl_jenis_perawatan` (
  `id_jenis` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `deskripsi` varchar(200) DEFAULT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_jenis_perawatan`
--

INSERT INTO `tbl_jenis_perawatan` (`id_jenis`, `nama`, `deskripsi`, `update_at`) VALUES
(1, 'Perawatan Umum', 'Perawatan Umum', '2021-10-27 19:16:20'),
(2, 'Scalling', 'Pembersihan Karang Gigi', '2021-10-27 19:16:20'),
(3, 'Konservasi', 'Penambalan', '2021-10-27 19:16:23'),
(4, 'Ododentik', 'Perawatan Saluran Akar', '2021-10-27 19:16:23'),
(5, 'Pencabutan dan Bedah', 'Pencabutan dan Bedah Gigi atau Gusi', '2021-10-27 19:18:39'),
(6, 'Protesa', 'Gigi Palsu', '2021-10-27 19:19:24'),
(7, 'Gigi Palsu Lepasan', 'Gigi Palsu Lepasan', '2021-10-27 19:19:24'),
(8, 'Orthodontic', 'Merapikan Gigi', '2021-10-27 19:20:24');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori_stok`
--

CREATE TABLE `tbl_kategori_stok` (
  `id_kategori_stok` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_kategori_stok`
--

INSERT INTO `tbl_kategori_stok` (`id_kategori_stok`, `nama`, `deskripsi`, `update_at`) VALUES
(1, 'Obat', 'Stok Obat', '2021-11-01 17:54:18'),
(2, 'Produk', 'Stok Produk', '2021-11-01 17:54:18'),
(3, 'Bahan', 'Stok Bahan Produk', '2021-11-01 17:54:39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_perawatan`
--

CREATE TABLE `tbl_perawatan` (
  `id_perawatan` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `deskripsi` varchar(300) NOT NULL,
  `biaya` varchar(12) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_perawatan`
--

INSERT INTO `tbl_perawatan` (`id_perawatan`, `id_jenis`, `nama`, `deskripsi`, `biaya`, `satuan`, `status`, `update_at`) VALUES
(1, 1, 'Konsultasi tanpa tindakan', '-', '20000', 'pcs', 1, '2021-10-27 19:22:02'),
(2, 1, 'Konsultasi dengan pemeriksaan', '-', '50000', 'pcs', 1, '2021-10-28 02:38:11'),
(3, 1, 'Foto Rontgent Per Kasus', 'Foto Rontgent Per Kasus', '150000', 'pcs', 1, '2021-10-28 02:39:06'),
(4, 1, 'Dental Scanning', '-', '500000', 'paket', 1, '2021-10-28 02:42:19'),
(5, 2, 'Scalling Class A, Sedikit', 'Scalling Class A, Sedikit2', '500000', 'pcs', 1, '2021-10-28 02:43:53'),
(7, 2, 'Scalling Class B, Sedang', 'Pembersihan Gigi Sedang2', '500000', 'pcs', 1, '2021-11-02 03:35:57'),
(8, 2, 'Scalling Class C, Banyak', 'Pembersihan disertai stain seluruh gigi', '650000', 'paket', 1, '2021-11-05 16:13:01'),
(9, 2, 'Office Bleacing', 'Bleaching', '3500000', 'paket', 1, '2021-11-05 16:14:14'),
(10, 2, 'Home Bleaching', 'Bleaching', '2000000', 'paket', 1, '2021-11-05 16:15:00'),
(11, 2, 'Dentova Mouth Wash', 'Pencuci Mulut', '35000', 'pcs', 1, '2021-11-05 16:15:32'),
(12, 3, 'GIC Class A, Kecil', 'Penambalan Kecil', '250000', 'pcs', 1, '2021-11-05 16:16:13'),
(13, 3, 'GIC Class B, Sedang', 'Penambalan Sedang', '300000', 'pcs', 1, '2021-11-05 16:17:25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rekam_medis`
--

CREATE TABLE `tbl_rekam_medis` (
  `id_rekam_medis` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `tgl_kunjungan` date NOT NULL,
  `keluhan` text,
  `perawatan` tinytext,
  `penatalaksaan` enum('Rawat Jalan','Rawat Inap','Rujuk','Lainnya') DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `updated at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_rekam_medis`
--

INSERT INTO `tbl_rekam_medis` (`id_rekam_medis`, `id_pasien`, `id_dokter`, `tgl_kunjungan`, `keluhan`, `perawatan`, `penatalaksaan`, `status`, `updated at`) VALUES
(1, 23, 24, '2020-10-17', 'Sakit Gigi', '3', 'Rawat Jalan', 1, '2021-11-09 04:59:11'),
(3, 23, 24, '2020-10-21', 'Bau nafas dan karang gigi', '8', 'Rujuk', 1, '2021-11-09 04:59:11'),
(4, 21, 24, '2021-10-15', 'Lubang pada gigi', '12', 'Rawat Jalan', 1, '2021-11-09 04:59:11'),
(5, 21, 24, '2021-11-05', NULL, NULL, NULL, 1, '2021-11-09 05:25:55');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resep`
--

CREATE TABLE `tbl_resep` (
  `id_resep` int(11) NOT NULL,
  `id_rekam_medis` int(11) NOT NULL,
  `id_stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_resep`
--

INSERT INTO `tbl_resep` (`id_resep`, `id_rekam_medis`, `id_stok`) VALUES
(1, 3, 1),
(2, 3, 2),
(3, 3, 2),
(4, 1, 2),
(6, 1, 1),
(7, 4, 1),
(8, 4, 2),
(9, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_riwayat_stok`
--

CREATE TABLE `tbl_riwayat_stok` (
  `id_riwayat_stok` int(11) NOT NULL,
  `sku` varchar(6) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `stok` int(11) NOT NULL DEFAULT '0',
  `id_user` int(11) NOT NULL DEFAULT '1',
  `id_transaksi` varchar(10) DEFAULT NULL,
  `tgl_transaksi` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notes` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_riwayat_stok`
--

INSERT INTO `tbl_riwayat_stok` (`id_riwayat_stok`, `sku`, `status`, `stok`, `id_user`, `id_transaksi`, `tgl_transaksi`, `updated_at`, `notes`) VALUES
(1, 'OB01', 0, 3, 1, NULL, '2021-11-01 00:00:00', '2021-11-07 20:07:21', ''),
(2, 'OB02', 0, 6, 1, NULL, '2021-11-01 00:00:00', '2021-11-07 20:08:05', ''),
(3, 'OB02', 0, 1, 1, '-', '2021-11-08 05:09:40', '2021-11-07 22:09:40', 'Obat baru'),
(5, 'OB01', 0, 1, 1, '-', '2021-11-08 05:33:02', '2021-11-07 22:33:02', ''),
(6, 'OB01', 0, 1, 1, '-', '2021-11-08 05:35:57', '2021-11-07 22:35:57', ''),
(7, 'OB02', 0, 1, 1, '-', '2021-11-08 09:02:54', '2021-11-08 02:02:54', ''),
(8, 'OB02', 0, 2, 1, '-', '2021-11-08 09:03:39', '2021-11-08 02:03:39', ''),
(9, 'OB01', 0, 10, 1, '-', '2021-11-08 09:05:58', '2021-11-08 02:05:58', ''),
(10, 'OB01', 0, 1, 1, '-', '2021-11-08 09:07:00', '2021-11-08 02:07:00', ''),
(11, 'OB01', 0, 1, 1, '-', '2021-11-08 09:08:55', '2021-11-08 02:08:55', ''),
(12, 'OB01', 0, 2, 1, '-', '2021-11-08 09:14:36', '2021-11-08 02:14:36', ''),
(13, 'OB01', 0, 1, 1, '-', '2021-11-08 09:15:23', '2021-11-08 02:15:23', ''),
(14, 'OB01', 0, 2, 1, '-', '2021-11-08 09:19:04', '2021-11-08 02:19:04', ''),
(15, 'OB01', 1, 2, 1, '-', '2021-11-08 09:19:36', '2021-11-08 02:19:36', ''),
(16, 'OB01', 2, 2, 1, '-', '2021-11-08 09:19:42', '2021-11-08 02:19:42', ''),
(17, 'OB01', 0, 2, 1, '-', '2021-11-08 09:20:25', '2021-11-08 02:20:25', ''),
(18, 'OB01', 0, 2, 1, '-', '2021-11-08 09:22:39', '2021-11-08 02:22:39', ''),
(19, 'OB01', 0, 2, 1, '-', '2021-11-08 09:23:24', '2021-11-08 02:23:24', ''),
(20, 'OB01', 0, 2, 1, '-', '2021-11-08 09:24:17', '2021-11-08 02:24:17', ''),
(21, 'OB01', 0, 2, 1, '-', '2021-11-08 09:28:29', '2021-11-08 02:28:29', ''),
(22, 'OB01', 0, 1, 1, '-', '2021-11-08 09:28:52', '2021-11-08 02:28:52', ''),
(23, 'OB01', 0, 1, 1, '-', '2021-11-08 09:29:12', '2021-11-08 02:29:12', ''),
(24, 'OB01', 0, 2, 1, '-', '2021-11-08 09:29:33', '2021-11-08 02:29:33', ''),
(25, 'OB01', 0, 2, 1, '-', '2021-11-08 09:29:57', '2021-11-08 02:29:57', ''),
(26, 'OB01', 1, 2, 1, '-', '2021-11-08 09:35:27', '2021-11-08 02:35:27', ''),
(27, 'OB01', 0, 2, 1, '-', '2021-11-08 09:46:31', '2021-11-08 02:46:31', ''),
(28, 'OB01', 1, 4, 1, 'TF0011', '2021-11-08 09:47:04', '2021-11-08 02:47:04', 'Beli');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stok`
--

CREATE TABLE `tbl_stok` (
  `id_stok` int(11) NOT NULL,
  `sku` varchar(6) NOT NULL,
  `id_jenis` int(11) NOT NULL DEFAULT '1',
  `nama` varchar(200) NOT NULL,
  `stok` int(11) NOT NULL,
  `deskripsi` varchar(3000) DEFAULT NULL,
  `satuan` varchar(10) DEFAULT 'pcs',
  `biaya` varchar(12) NOT NULL DEFAULT '0',
  `tanggal_masuk` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT '1',
  `img` varchar(200) DEFAULT 'produk.jpg',
  `update_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_stok`
--

INSERT INTO `tbl_stok` (`id_stok`, `sku`, `id_jenis`, `nama`, `stok`, `deskripsi`, `satuan`, `biaya`, `tanggal_masuk`, `status`, `img`, `update_at`) VALUES
(1, 'OB01', 1, 'Ponstan', 10, 'Ponstan sering digunakan untuk mengobati rasa sakit mulai dari sakit gigi sampai cedera otot bahkan sakit setelah melahirkan.', 'pcs', '320000', '2021-10-04 17:00:00', 1, 'Ponstan-300x300.jpg', '2021-11-01 18:26:03'),
(2, 'OB02', 1, 'Ibuprofen', 20, 'Ibuprofen merupakan golongan obat-obatan yang mampu meringankan rasa sakit dan meredakan inflamasi atau peradangan.', 'pcs', '210000', '2021-10-31 17:00:00', 1, 'Ibuprofen.jpg', '2021-11-05 18:28:02'),
(3, 'OB03', 1, 'Dentasol', 2, 'Dalam obat ini terdapat zat anestesi topikal dengan antiseptik yang dapat diaplikasikan secara langsung pada gigi yang sakit.\n\nKandungan zat tersebut mampu mengurangi rasa nyeri akibat sakit gigi juga mampu mengobati infeksi jamur dan bakteri lho, Toppers.', 'pcs', '45000', '2021-11-05 17:00:00', 1, 'Dentasol.jpg', '2021-11-06 00:52:38'),
(13, 'OB04', 1, 'Obat Sakit Gigi Cap Burung Kakak Tua', 0, 'Obat sakit gigi Cap Burung Kakak Tua ini mengandung senyawa gliserin, ethanol, creosote, dan oleum caryophylli.  Kandungan tersebut berguna untuk mengatasi sakit gigi, gusi bengkak, dan membersihkan gigi dari kuman yang menempel.  Untuk menggunakan obat sakit gigi bisa langsung ditempelkan menggunakan kapas atau dengan cara dikumur. ', 'pcs', '250000', '2021-11-02 17:00:00', 1, 'burungkaka.jpg', '2021-11-08 19:31:05'),
(14, 'PB01', 2, 'Veneer', 0, 'veneer adalah lapisan bahan yang ditempatkan di atas gigi', 'pcs', '1000000', '2021-11-08 20:12:34', 1, 'veener.jpg', '2021-11-08 20:12:34'),
(15, 'BO01', 3, 'Zirconia', 0, 'Zirconium merupakan logam yang bewarna putih keabuan dan sangat jarang ditemukan di alam bebas', 'pcs', '980000', '2021-11-08 20:17:20', 1, 'zirconia.jpg', '2021-11-08 20:17:20'),
(16, 'BO02', 3, 'Porcelain', 0, 'Bahan yang digunakan Full porcelain / Emax bahan khusus untuk membuat gigi tiruan permanent.', 'pcs', '560000', '2021-11-08 20:19:52', 1, 'porcelain.jpeg', '2021-11-08 20:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `designation`, `description`, `image`, `status`, `created_at`) VALUES
(1, 'Team Php', 'Developer1', '<p><tt><span style=\"font-family:Courier New,Courier,monospace\">Team -- Integer tincidunt odio et imperdiet luctus. Donec id sapien est. Ut consequat magna nunc, sit amet mollis nulla pretium ac. Proin rhoncus, sem id aliquet interdum, orci augue bibendum sem, id venenatis nisi sapien quis ipsum. Sed mollis finibus eros, in cursus nulla tempor ut. Mauris pretium sapien sit amet ultricies consectetur. Nullam nisl massa, condimentum nec nunc sit amet, ullamcorper mattis metus. Sed ut malesuada nulla, vel hendrerit magna. </span></tt></p>\r\n', 'Penguins.jpg', 2, '2017-08-16 19:09:34');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `domain`, `description`, `image`, `status`, `created_at`) VALUES
(1, 'New Testimonial', 'www.testimonial.com', '<p><strong>Edited </strong>-- Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque orci ex, finibus vitae nunc eu, accumsan luctus metus. Curabitur magna sapien, porta et vulputate id, finibus et sapien. Fusce a varius leo, eget vestibulum velit. Ut tristique, arcu ac scelerisque iaculis, elit quam dictum sem, in sagittis justo augue sed sapien. Pellentesque mollis orci in consequat euismod. Donec sodales nisi ut diam tempus viverra. Aliquam eu efficitur velit, a sollicitudin enim. Nulla posuere ullamcorper dolor quis dapibus. Vivamus maximus purus in urna feugiat, vel ornare felis tristique. Mauris pretium faucibus metus, ut pharetra ligula bibendum ultricies. Nullam volutpat turpis vitae interdum varius. Quisque viverra dictum magna nec eleifend. Pellentesque a purus purus.</p>\r\n\r\n<p>Nunc eget vestibulum mi. In hac habitasse platea dictumst. Fusce vitae mauris sed eros volutpat porta. Sed blandit ultrices velit nec porta. Suspendisse lobortis nisl ut nisi viverra fermentum. Duis volutpat pretium consectetur. Morbi ornare ante metus. Phasellus vitae erat enim. Cras vulputate congue neque a suscipit. Integer facilisis fringilla gravida. Mauris id molestie neque, quis varius tellus. Aenean eu nisl et justo accumsan feugiat. Sed in porttitor mauris. Etiam non dui ac ipsum ullamcorper imperdiet. Etiam eget viverra enim, vel tempus urna.</p>\r\n', 'Lighthouse.jpg', 0, '2017-08-17 14:34:03'),
(3, 'New Testimonial2', 'www.testimonial2.com', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque orci ex, finibus vitae nunc eu, accumsan luctus metus. Curabitur magna sapien, porta et vulputate id, finibus et sapien. Fusce a varius leo, eget vestibulum velit. Ut tristique, arcu ac scelerisque iaculis, elit quam dictum sem, in sagittis justo augue sed sapien. Pellentesque mollis orci in consequat euismod. Donec sodales nisi ut diam tempus viverra. Aliquam eu efficitur velit, a sollicitudin enim. Nulla posuere ullamcorper dolor quis dapibus. Vivamus maximus purus in urna feugiat, vel ornare felis tristique. Mauris pretium faucibus metus, ut pharetra ligula bibendum ultricies. Nullam volutpat turpis vitae interdum varius. Quisque viverra dictum magna nec eleifend. Pellentesque a purus purus.</p>\r\n\r\n<p>Nunc eget vestibulum mi. In hac habitasse platea dictumst. Fusce vitae mauris sed eros volutpat porta. Sed blandit ultrices velit nec porta. Suspendisse lobortis nisl ut nisi viverra fermentum. Duis volutpat pretium consectetur. Morbi ornare ante metus. Phasellus vitae erat enim. Cras vulputate congue neque a suscipit. Integer facilisis fringilla gravida. Mauris id molestie neque, quis varius tellus. Aenean eu nisl et justo accumsan feugiat. Sed in porttitor mauris. Etiam non dui ac ipsum ullamcorper imperdiet. Etiam eget viverra enim, vel tempus urna.</p>\r\n', 'supPro.png', 1, '2017-08-17 15:27:09');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `golongan_darah` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `username`, `contact`, `address`, `gender`, `image`, `role_id`, `golongan_darah`, `dob`, `status`, `register_date`) VALUES
(1, 'Citra', 'citra@halawa.com', 'e10adc3949ba59abbe56e057f20f883e', 'citra', '082276438906', 'Jl Gelas 12A Medan', 'Female', 'YADU_Logo.JPG', 1, 'O', '1998-08-03', 1, '2021-10-27 09:44:43'),
(4, 'drg. Kelvin Tantono', 'kelvin@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'kelvin', '9898989898', 'Medan', 'Male', 'slide_05.jpg', 2, 'O', '1990-08-03', 1, '2019-08-13 18:49:15'),
(21, 'budi', 'budi@mail.com', 'e10adc3949ba59abbe56e057f20f883e', 'budi', '0999', 'jalan', 'Male', 'ktp_cover.jpeg', 4, 'A', '2021-10-08', 1, '2021-10-27 11:06:56'),
(22, 'Setriani', 'setriani@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'setriani', '09888', 'Medan', 'Male', 'noimage.jpg', 4, 'O', '1999-02-02', 1, '2021-11-08 13:23:01'),
(23, 'deswenti', 'deswenti@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'deswenti', '0992889', 'Medan', 'Female', 'user.jpeg', 4, 'A', '1992-05-05', 1, '2021-11-08 13:28:47'),
(24, 'drg. Fenny Valentine Gouw', 'fenny@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'fenny', '09988789', 'Medan', 'Female', 'fenny.jpg', 3, 'O', '1993-02-03', 1, '2021-11-08 13:33:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_content`
--
ALTER TABLE `page_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`user_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sco`
--
ALTER TABLE `sco`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_config`
--
ALTER TABLE `site_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders_img`
--
ALTER TABLE `sliders_img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sociallinks`
--
ALTER TABLE `sociallinks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_jenis_perawatan`
--
ALTER TABLE `tbl_jenis_perawatan`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `tbl_kategori_stok`
--
ALTER TABLE `tbl_kategori_stok`
  ADD PRIMARY KEY (`id_kategori_stok`);

--
-- Indexes for table `tbl_perawatan`
--
ALTER TABLE `tbl_perawatan`
  ADD PRIMARY KEY (`id_perawatan`);

--
-- Indexes for table `tbl_rekam_medis`
--
ALTER TABLE `tbl_rekam_medis`
  ADD PRIMARY KEY (`id_rekam_medis`),
  ADD KEY `id_dokter` (`id_dokter`),
  ADD KEY `id_pasien` (`id_pasien`);

--
-- Indexes for table `tbl_resep`
--
ALTER TABLE `tbl_resep`
  ADD PRIMARY KEY (`id_resep`),
  ADD KEY `id_obat` (`id_stok`),
  ADD KEY `id_berobat` (`id_rekam_medis`);

--
-- Indexes for table `tbl_riwayat_stok`
--
ALTER TABLE `tbl_riwayat_stok`
  ADD PRIMARY KEY (`id_riwayat_stok`);

--
-- Indexes for table `tbl_stok`
--
ALTER TABLE `tbl_stok`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `page_content`
--
ALTER TABLE `page_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `sco`
--
ALTER TABLE `sco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `site_config`
--
ALTER TABLE `site_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders_img`
--
ALTER TABLE `sliders_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sociallinks`
--
ALTER TABLE `sociallinks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_jenis_perawatan`
--
ALTER TABLE `tbl_jenis_perawatan`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_kategori_stok`
--
ALTER TABLE `tbl_kategori_stok`
  MODIFY `id_kategori_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_perawatan`
--
ALTER TABLE `tbl_perawatan`
  MODIFY `id_perawatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_rekam_medis`
--
ALTER TABLE `tbl_rekam_medis`
  MODIFY `id_rekam_medis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_resep`
--
ALTER TABLE `tbl_resep`
  MODIFY `id_resep` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_riwayat_stok`
--
ALTER TABLE `tbl_riwayat_stok`
  MODIFY `id_riwayat_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_stok`
--
ALTER TABLE `tbl_stok`
  MODIFY `id_stok` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
